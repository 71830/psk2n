#include "serverexception.h"

ServerException::ServerException() : Exception::Exception()
{

}

ServerException::ServerException(int errorNumber, std::string errorMessage) : Exception::Exception(errorNumber, errorMessage)
{

}
