#ifndef COMMANDREADER_H
#define COMMANDREADER_H
#include "command.h"
#include <errno.h>
#include <string>
class CommandReader
{
public:
    CommandReader(int fd);
    Command * readCommand();
    Command * readCommand(int filedescriptor);
    int getError();
    char * getErrorMessage();
private:
    int fd;
    int error;
    char * errmsg;
    char * readMessage(int filedescriptor, int sizeOfFirstRead);
    char * readUserName(int filedescriptor);
    CommandType readCommandType(int filedescriptor);

};

#endif // COMMANDREADER_H
