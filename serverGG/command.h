#ifndef COMMAND_H
#define COMMAND_H
#include <commandenum.h>
#include <stddef.h>
#include <string>
class Command
{
private:
      CommandType type;
      const char * message ;
      const char * from  ;
      const char * to ;
      //from
      //to



public:
  //  Command(const char * message, CommandType commandType, const char * from, const char * to);
    Command(const std::string message, CommandType commandType, const std::string from, std::string to);
    Command(){;}
    ~Command();

    bool validate();

    CommandType getType();

    const char * getMessage();
    const char * getFrom();
    const char * getTo();



};

#endif // COMMAND_H
