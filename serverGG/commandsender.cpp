#include "commandsender.h"
#include "unistd.h"
#include "statics.h"
#include "string.h"
#include <string>
CommandSender::CommandSender(int fd)
{
    this->fd = fd;
}


int CommandSender::sendCommand(Command *cmd){
    int k;
    if((k = write(fd, std::to_string(cmd->getType()).c_str(), CommandTypeSize)) < 0) return k;
    if((k = write(fd, std::to_string(strlen(cmd->getMessage())).c_str(), MessageBufSize)) < 0) return k;
    if((k = write(fd, cmd->getMessage(), strlen(cmd->getMessage()))) < 0) return k;
    if((k = write(fd, cmd->getFrom(), UserNameBufSize)) < 0) return k;
    if((k = write(fd, cmd->getTo(), UserNameBufSize)) < 0) return k;
    return 0;

}
