#include "clientsmanager.h"
#include <string.h>
#include <sqlitecontroller.h>


ClientsManager::ClientsManager()
{
    this->clientsMapByName = new unordered_map<string, ServerClient*>();
    this->clientsMapByFd= new unordered_map<int, ServerClient*>();
}

ClientsManager::~ClientsManager(){
    delete this->clientsMapByName;
    delete this->clientsMapByFd;
}

void ClientsManager::addByFd(ServerClient *client){
    this->clientsMapByFd->insert({client->getSocket()->getDescriptor(), client});
}

void ClientsManager::addByUsername(ServerClient *client){
    this->clientsMapByName->insert({client->getUsername(), client});
}



bool ClientsManager::remove(ServerClient * client){ //change it
    this->clientsMapByName->erase(client->getUsername());
    this->clientsMapByFd->erase(client->getSocket()->getDescriptor());
    return true;
}

int ClientsManager::getClientsCount(){
    return this->clientsMapByName->size();
}

ServerClient * ClientsManager::getClientByName(std::string username){
    unordered_map<std::string, ServerClient*>::const_iterator search = this->clientsMapByName->find(username);
    if(search != this->clientsMapByName->end()){
        return search->second;
    }
    return nullptr;
}

ServerClient * ClientsManager::getClientByFd(int fd){

    auto search = this->clientsMapByFd->find(fd);
    if(search != this->clientsMapByFd->end()){
        return search->second;
    }
    return nullptr;
}


unordered_map<std::string, ServerClient*> *ClientsManager::getClientsByName(){
    return this->clientsMapByName;
}

unordered_map<int, ServerClient*> *ClientsManager::getClientsByFd(){
    return this->clientsMapByFd;
}

std::vector<ServerClient*> ClientsManager::getAllLoggedUsersFromList(std::vector<std::string> list){

    std::vector<ServerClient*> result;
    for(std::vector<std::string>::iterator it = list.begin(); it != list.end(); ++it){
        ServerClient * client = getClientByName(*it);
        if(client != nullptr && client->isLogged()){
            result.push_back(client);
        }
    }
    return result;
}

std::string ClientsManager::prepareToSendMembersAndLogged(std::string &username){
    std::vector<std::string> list = SqliteController::getInstance().getUserMembers(username);
    std::string result;
    std::string logged;
    for(std::vector<std::string>::iterator it = list.begin(); it != list.end(); ++it){
        ServerClient * client = getClientByName(*it);
        if(client != nullptr && !client->getUsername().empty()){
            logged = client->isLogged() ? "1" : "0";
            result.append("||");
            result.append(client->getUsername());
            result.append("&");
            result.append(logged);
        } else {
            result.append("||");
            result.append(*it);
            result.append("&");
            result.append("0");
        }
    }
    if(result.empty()) result = "||";
    return result;
}
