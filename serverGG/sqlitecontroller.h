#ifndef SQLITECONTROLLER_H
#define SQLITECONTROLLER_H

#include <sqlite3.h>
#include <iostream>
#include <string>
#include <vector>
#include <sssqlitecontroller.h>
class SqliteController : public SSSqliteController
{
public:
    ~SqliteController();

    static SqliteController& getInstance();


    void insertNewUser(std::string &name, std::string &password);
    bool userExist(std::string &name);
    bool checkCredentials(std::string &name, std::string &password);

    std::string getUsersNameByFilterName(std::string &name);
    std::vector<std::string> getUserMembers(std::string &name);
    std::vector<std::string> getUsersThatUserMember(std::string &membername);
    bool addMessage(std::string &msgFrom, std::string &msgTo, std::string &msg, int sent);
    void setMessageSent(int messageId, int sent);
    std::vector<std::string> getMessages(std::string & userTo, int sent);

    bool addUserMember(std::string & name, std::string & membername);
    bool removeUserMember(std::string & name, std::string & membername);

private:
    SqliteController(std::string & dbname, std::string &createQuery);



};

#endif // SQLITECONTROLLER_H
