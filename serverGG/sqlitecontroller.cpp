#include "sqlitecontroller.h"
#include "exception.h"
#include <sys/stat.h>

SqliteController::SqliteController(std::string & dbname, std::string & createQuery) : SSSqliteController(dbname, createQuery)
{

}



void SqliteController::insertNewUser(std::string &name, std::string &password){

    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "insert into users (name, password) values(?, ?);", -1, &stmt, 0);

    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, password.c_str(), -1, SQLITE_STATIC);

    if(result == SQLITE_OK){
        result = sqlite3_step(stmt);

        //std::string s = std::string(sqlite3_errstr(result));
        if(result == 19){
                std::string s("User with this name exists!");
                throw Exception(result, s);
        }
    } else {
        std::string s = std::string(sqlite3_errstr(result));
        throw Exception(result, s);
    }
}

bool SqliteController::userExist(std::string &name){

    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "select name from users where name = ?;", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);

    if(result != SQLITE_OK){
        std::string s(sqlite3_errstr(result));
        sqlite3_finalize(stmt);
        throw Exception(result, s);
    }

    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    return result == SQLITE_ROW;
}

bool SqliteController::checkCredentials(std::string &name, std::string &password){
    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "select name from users where name = ? and password = ?;", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, password.c_str(), -1, SQLITE_STATIC);


    if(result != SQLITE_OK){
        std::string s(sqlite3_errstr(result));
        sqlite3_finalize(stmt);
        throw Exception(result, s);
    }

    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return result == SQLITE_ROW;
}


std::vector<std::string> SqliteController::getUserMembers(std::string &name){

    sqlite3_stmt * stmt;
    const char * query = "Select membername from members where username = ?;";
    int result = sqlite3_prepare_v2(getDB(), query , strlen(query), &stmt, 0);

    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);


    std::vector<std::string> results;

    if(result != SQLITE_OK){
        sqlite3_finalize(stmt);
        return results;
    }

    result = sqlite3_step(stmt);
    int cols = sqlite3_column_count(stmt);

    while(result == SQLITE_ROW){
        for(int i = 0; i < cols; i++){
            results.push_back(reinterpret_cast<const char*>(sqlite3_column_text(stmt, i)));
        }
        result = sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);

    return results;
}

std::vector<std::string> SqliteController::getUsersThatUserMember(std::string &membername){
    sqlite3_stmt * stmt;
    const char * query = "Select username from members where membername = ?;";
    int result = sqlite3_prepare_v2(getDB(), query , strlen(query), &stmt, 0);

    sqlite3_bind_text(stmt, 1, membername.c_str(), -1, SQLITE_STATIC);


    std::vector<std::string> results;

    if(result != SQLITE_OK){
        sqlite3_finalize(stmt);
        return results;
    }

    result = sqlite3_step(stmt);
    int cols = sqlite3_column_count(stmt);

    while(result == SQLITE_ROW){
        for(int i = 0; i < cols; i++){
            results.push_back(reinterpret_cast<const char*>(sqlite3_column_text(stmt, i)));
        }
        result = sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);

    return results;
}

bool SqliteController::addMessage(std::string &msgFrom, std::string &msgTo, std::string &msg, int sent){
    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "insert into messages (userfromname, usertoname, message, sent) values (?, ?, ?, ?);", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, msgFrom.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, msgTo.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 3, msg.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 4, sent);

    if(result != SQLITE_OK){
        return false;
    }

    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return result == SQLITE_DONE;
}

void SqliteController::setMessageSent(int messageId, int sent){
    sqlite3_stmt * stmt;

    int result = sqlite3_prepare_v2(getDB(), "update messages set sent = ? where id = ?;", -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, sent);
    sqlite3_bind_int(stmt, 2, messageId);

    if(result == SQLITE_OK){
        result = sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);
}
std::vector<std::string> SqliteController::getMessages(std::string & userTo, int sent){

    sqlite3_stmt * stmt;

    int result = sqlite3_prepare_v2(getDB(), "select id, userfromname, usertoname, message from messages where usertoname = ? and sent = ? ;",-1, &stmt ,0);
    sqlite3_bind_text(stmt, 1, userTo.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 2, sent);

    std::vector<std::string> results;

    if(result != SQLITE_OK){
        sqlite3_finalize(stmt);
       return results;
    }

    result = sqlite3_step(stmt);
    int cols = sqlite3_column_count(stmt);

    while(result == SQLITE_ROW){
        std::string row;
        for(int i = 0; i < cols; i++){
            row.append(reinterpret_cast<const char *>(sqlite3_column_text(stmt, i)));
            row.append("||");
        }
        results.push_back(row);
        result = sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);
    return results;
}



std::string SqliteController::getUsersNameByFilterName(std::string &name){

    sqlite3_stmt * stmt;
    std::string str("%");
    str.append(name);
    str.append("%");

    int result = sqlite3_prepare_v2(getDB(),"select name from users where name like ? limit 10;", -1, &stmt, 0);

    sqlite3_bind_text(stmt, 1, str.c_str(), -1, SQLITE_STATIC);

    std::string strResult;

    if(result == SQLITE_OK){
        result = sqlite3_step(stmt);

        while(result == SQLITE_ROW){
            strResult.append("||");
            strResult.append(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));

            result = sqlite3_step(stmt);
        }

    }
    sqlite3_finalize(stmt);
    return strResult;
}

bool SqliteController::addUserMember(std::string & name, std::string & membername){
    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "insert into members (username, membername) values (?, ?);", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, membername.c_str(), -1, SQLITE_STATIC);

    if(result != SQLITE_OK){
        return false;
    }

    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return result == SQLITE_DONE;
}

bool SqliteController::removeUserMember(std::string & name, std::string & membername){
    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "delete from members where username = ? and  membername = ?;", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, membername.c_str(), -1, SQLITE_STATIC);

    if(result != SQLITE_OK){
        return false;
    }

    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return result == SQLITE_DONE;
}

SqliteController::~SqliteController(){

}

SqliteController& SqliteController::getInstance()
{
    static std::string query = "CREATE TABLE users(id integer primary key autoincrement, name varchar(20) not null unique, password varchar(20) not null);"
            "CREATE TABLE members(username varchar(20) not null, membername varchar(20) not null, primary key(username, membername),"
            "foreign key(username) references users(name), foreign key(membername) references users(name));"
            "CREATE TABLE messages(id integer primary key autoincrement, userfromname varchar(20) not null, usertoname varchar(20) not null, message varchar(300), timestamp datetime default current_timestamp, sent integer, foreign key(userfromname)"
            "references users(name), foreign key(usertoname) references users(name), unique(userfromname, usertoname, timestamp));";

    std::string dbname = "serverss.db";
    static SqliteController controll(dbname, query);
    return controll;
}


