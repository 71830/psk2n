#include "serverclient.h"



ServerClient::ServerClient(Socket * socket)
{
    this->socket = socket;
    this->logged = false;
}

 ServerClient::~ServerClient(){
     delete socket;
 }

Socket * ServerClient::getSocket(){
    return this->socket;
}
std::string ServerClient::getUsername(){
    return this->username;
}

void ServerClient::setUsername(std::string username){
    this->username = username;
}

void ServerClient::setLogged(bool value){
    this->logged = value;
}

bool ServerClient::isLogged(){
    return this->logged;
}
