#ifndef SQLITECONTROLLER_H
#define SQLITECONTROLLER_H

#include <sqlite3.h>
#include <iostream>
#include <string>
class SqliteController
{
public:
    ~SqliteController();

    static SqliteController& getInstance();
    sqlite3 *getDB();

    void insertNewUser(std::string &name, std::string &password);
    bool userExist(std::string &name);
    bool checkCredentials(std::string &name, std::string &password);

    std::string getUsersNameByFilterName(std::string name);
private:
    SqliteController();
    sqlite3 *db;
    char ** getTable(std::string sqlstatement, int & rows, int & cols);
    bool databaseExists(const std::string& name);


};

#endif // SQLITECONTROLLER_H
