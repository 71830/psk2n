#ifndef SERVERCLIENT_H
#define SERVERCLIENT_H
#include <sys/socket.h>
#include <netinet/in.h>
#include <socket.h>
#include <string>
class ServerClient
{
private:
    Socket * socket;
    std::string username;
    bool logged;
public:
    ServerClient(Socket * socket);
    ~ServerClient();
    Socket * getSocket();
    std::string getUsername();

    void setUsername(std::string username);
    void setLogged(bool value);
    bool isLogged();
};
#endif // SERVERCLIENT_H
