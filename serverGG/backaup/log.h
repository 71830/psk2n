#ifndef LOG_H
#define LOG_H
#include <string>
#include <iostream>

class Log
{
private:
    static void m(std::string title, std::string message);
public:
    Log();
    static void i(std::string title, std::string message);
    static void d(std::string title, std::string message);
    static void e(std::string title, std::string message);
    static void m(std::string message);
};

#endif // LOG_H
