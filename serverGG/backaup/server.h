#ifndef SERVER_H
#define SERVER_H

#include<iostream>

#include <clientsmanager.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<errno.h>

#include <serverexception.h>
#include <newepoll.h>
#include <socket.h>

#include <string>
#include <command.h>

class Server
{
private:

        Socket * socket;
        ClientsManager * clientsManager;
        NewEpoll * eventPoll;


        int listenQueueSize;

        bool acceptLoopCanRun;

        void acceptLoop();
        void acceptClient();
        void createNewEpollAndAddAcceptEvent();
        void addNewClient(int acceptValue, sockaddr_in * sockAddr);
        void removeClient(ServerClient * client);

        bool executeCommand(Command *cmd, int fd);
        void sendBroadcastMessage(Command *cmd);
        void registerUser(std::string &name, std::string &password);





public:
    Server(const char * serverIp, int port, int listenQueueSize);
    ~Server();
    ClientsManager * getClientsMenager();
    Socket * getSocket();
    NewEpoll * getEventPoll();

    void run();

};

#endif // SERVER_H
