#ifndef CLIENTSMANAGER_H
#define CLIENTSMANAGER_H
#include <serverclient.h>
#include <sys/epoll.h>
#include <thread>
#include <errno.h>
#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;
class ClientsManager
{
private:
    unordered_map<std::string, ServerClient*> * clientsMapByName;
    unordered_map<int, ServerClient*> * clientsMapByFd;

    void epollLoop();
public:
    ClientsManager();
    ~ClientsManager();

    void addByFd(ServerClient * client);
    void addByUsername(ServerClient * client);
    bool remove(ServerClient * client);
    int getClientsCount();
    ServerClient * getClientByName(string username);
    ServerClient * getClientByFd(int fd);
    unordered_map<std::string, ServerClient*> *getClientsByName();
    unordered_map<int, ServerClient*> *getClientsByFd();

};

#endif // CLIENTSMANAGER_H
