#include "server.h"
#include "serverclient.h"
#include "unistd.h"
#include <statics.h>
#include <sqlitecontroller.h>
#include <commandreader.h>
#include <commandsender.h>
#include <cassert>
#include <unordered_map>
#include <log.h>

Server::Server(const char * serverIp, int port, int listenQueueSize)
{
    SqliteController::getInstance();


    if(listenQueueSize <= 0){
        throw ServerException(0, "Listen queue size must be greater than 0.");
    }

    try{
        this->socket = new Socket(serverIp, port);
    } catch(Exception & ex){
        throw ServerException(ex.getErrorNumber(), ex.getErrorMessage());

    }

    this->clientsManager = new ClientsManager();
    this->acceptLoopCanRun = true;

}

Server::~Server(){
    delete this->socket;
    delete this->clientsManager;
}

void Server::run()
{
   this->getSocket()->bindSocket();
   this->getSocket()->startListen(this->listenQueueSize);
   this->acceptLoop();
}


void Server::acceptLoop(){ 

    this->createNewEpollAndAddAcceptEvent();

    epoll_event e;

    while(this->acceptLoopCanRun && this->getEventPoll()->wait(&e, 1, -1)){
        if(e.data.fd == this->getSocket()->getDescriptor()){
            this->acceptClient();
        } else if (getClientsMenager()->getClientByFd(e.data.fd) != nullptr){
            if(e.events & EPOLLRDHUP){
                removeClient(getClientsMenager()->getClientByFd(e.data.fd));
                //send message to all that user is not active anymore;
            }
            else if(e.events == EPOLLIN){
                Command * cmd = CommandReader(e.data.fd).readCommand();
                executeCommand(cmd, e.data.fd);

            }
        } else {
            assert(false);
        }

    }
    this->getSocket()->closeSocket();
}

/*
void Server::sendBroadcastMessage(Command &cmd){
    for(unordered_map<std::string, ServerClient*>::iterator it= getClientsMenager()->getClientsByName()->begin(); it != getClientsMenager()->getClientsByName()->end();++it){
        CommandSender((*it).second->getSocket()->getDescriptor()).sendCommand(cmd);
    }
}
*/
bool Server::executeCommand(Command *cmd, int fd){
    if(!cmd->validate()) return false;

    switch (cmd->getType()) {
    case CommandType::REGISTER:
        {
            ServerClient* client = getClientsMenager()->getClientByFd(fd);
            if(client != nullptr){
                try{

                    std::string name(cmd->getFrom());
                    std::string pass(cmd->getMessage());

                    SqliteController::getInstance().insertNewUser(name, pass);
                    client->setUsername(cmd->getFrom());
                    getClientsMenager()->addByUsername(client);

                    Command * servCmd = new Command("Success", CommandType::REGISTER_SUCCESS, "server", cmd->getFrom());
                    CommandSender(client->getSocket()->getDescriptor()).sendCommand(servCmd);

                    Log::i("Server", "User " + name + " has been registered!");

                } catch (Exception &ex){

                    char * x = (char *)malloc(ex.getErrorMessage().size()); /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    strcpy(x, ex.getErrorMessage().c_str());

                    Command* servCmd = new Command(x, CommandType::REGISTER_FAILED, "server", cmd->getFrom());
                    CommandSender(client->getSocket()->getDescriptor()).sendCommand(servCmd);
                    Log::e("Server", "Cannot register new user " + ex.getErrorMessage());
                    Log::d("Server", ex.getSystemErrorMessage());
                }
            }
            break;
        }
    case CommandType::LOG_IN:
        {

            ServerClient* loggedClient = getClientsMenager()->getClientByName(std::string(cmd->getFrom()));
            ServerClient* client = getClientsMenager()->getClientByFd(fd);

            Command * servCmd;

            if(client != nullptr){

                try{
                    std::string name(cmd->getFrom());
                    std::string pass(cmd->getMessage());

                    if(loggedClient != nullptr && loggedClient->isLogged()){
                        servCmd = new Command("Bad login request user with this username is already logged in.", CommandType::LOGGING_FAILED, "server", cmd->getFrom());
                        Log::e("Server","Bad login request user with this username is already logged in. Error for descriptor: " + std::to_string(client->getSocket()->getDescriptor()) + " with user: " + std::string(cmd->getFrom()));

                    } else if(SqliteController::getInstance().checkCredentials(name, pass)){
                        client->setUsername(std::string(cmd->getFrom()));
                        getClientsMenager()->addByUsername(client);
                        servCmd = new Command("Success", CommandType::LOGGING_SUCCESS, "server", cmd->getFrom());
                        client->setLogged(true);
                        Log::i("Server", "User " + name + " has been logged in!");

                    } else {
                        servCmd = new Command("Failed. User does not exists or wrong password.", CommandType::LOGGING_FAILED, "server", cmd->getFrom());
                        Log::e("Server", "Bad login request from descriptor " + std::to_string(client->getSocket()->getDescriptor())  + " with name " + name);

                    }
                } catch (Exception &ex){
                    servCmd = new Command("Failed. Database problem found. Try again.", CommandType::LOGGING_FAILED, "server", cmd->getFrom());
                    Log::e("Server", "Database bad login request from descriptor: " + std::to_string(client->getSocket()->getDescriptor()) + " with user: " + std::string(cmd->getFrom()));
                }
                CommandSender(client->getSocket()->getDescriptor()).sendCommand(servCmd);
            }
            break;
        }
    case CommandType::SEND_MESSAGE:
        {
            ServerClient* client = getClientsMenager()->getClientByName(cmd->getTo());
            if(client != nullptr){
               CommandSender(client->getSocket()->getDescriptor()).sendCommand(cmd);

            }
            Log::m("Sending from " +  std::string(cmd->getFrom()) + " to " + std::string(cmd->getTo()));
            break;
        }
    case CommandType::USERS_SEARCH:
        {
            std::string users = SqliteController::getInstance().getUsersNameByFilterName(cmd->getMessage());
            CommandType cmdType = CommandType::USERS_SEARCH;
            if(users.empty()){ users = std::string("No users found"); cmdType = CommandType::USERS_SEARCH_NOT_FOUND; }
            Command * sendUsersCmd = new Command(users.c_str(), cmdType, "Server", cmd->getFrom());
            CommandSender(fd).sendCommand(sendUsersCmd);

            Log::i("Server", "Sending searched users to " + std::string(cmd->getFrom()));
        }
    default:
        break;
    }
    return true;
}

void Server::acceptClient(){
    sockaddr_in clientSocket;
    socklen_t socketSize;

    int acceptValue = accept(this->getSocket()->getDescriptor(), (sockaddr*)&clientSocket, &socketSize);
    if(acceptValue > 0){
        this->addNewClient(acceptValue, &clientSocket);
    }
}

void Server::createNewEpollAndAddAcceptEvent(){
    try{
        this->eventPoll = new NewEpoll(0);
        this->getEventPoll()->addEvent(this->getSocket()->getDescriptor(), EPOLLIN);
    } catch (Exception & ex){
        throw ServerException(ex.getErrorNumber(), ex.getErrorMessage());
    }
}

void Server::addNewClient(int acceptValue, sockaddr_in * sockAddr){
    try{
       this->getEventPoll()->addEvent(acceptValue, EPOLLIN | EPOLLRDHUP);
       try{
            ServerClient * client = new ServerClient(new Socket(acceptValue, *sockAddr));

            this->getClientsMenager()->addByFd(client);

        }catch(Exception & ex){
            this->getEventPoll()->removeEvent(acceptValue);
            ex.printError();
        }
    } catch (Exception  &ex){
        ex.printError();
    }
}

void Server::removeClient(ServerClient * client){
    this->getEventPoll()->removeEvent(client->getSocket()->getDescriptor());
    this->getClientsMenager()->remove(client);
    Log::i("Server", "Client " + client->getUsername() + " has been removed.");
}

Socket * Server::getSocket(){
    return this->socket;
}


ClientsManager * Server::getClientsMenager(){
    return this->clientsManager;
}

NewEpoll * Server::getEventPoll(){
    return this->eventPoll;
}
