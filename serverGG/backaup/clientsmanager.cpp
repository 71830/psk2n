#include "clientsmanager.h"
#include <string.h>


ClientsManager::ClientsManager()
{
    this->clientsMapByName = new unordered_map<string, ServerClient*>();
    this->clientsMapByFd= new unordered_map<int, ServerClient*>();
}

ClientsManager::~ClientsManager(){
    delete this->clientsMapByName;
    delete this->clientsMapByFd;
}

void ClientsManager::addByFd(ServerClient *client){
    this->clientsMapByFd->insert({client->getSocket()->getDescriptor(), client});
}

void ClientsManager::addByUsername(ServerClient *client){
    this->clientsMapByName->insert({client->getUsername(), client});
}



bool ClientsManager::remove(ServerClient * client){ //change it
    this->clientsMapByName->erase(client->getUsername());
    this->clientsMapByFd->erase(client->getSocket()->getDescriptor());
    return true;
}

int ClientsManager::getClientsCount(){
    return this->clientsMapByName->size();
}

ServerClient * ClientsManager::getClientByName(std::string username){
    unordered_map<std::string, ServerClient*>::const_iterator search = this->clientsMapByName->find(username);
    if(search != this->clientsMapByName->end()){
        return search->second;
    }
    return nullptr;

}

ServerClient * ClientsManager::getClientByFd(int fd){

    auto search = this->clientsMapByFd->find(fd);
    if(search != this->clientsMapByFd->end()){
        return search->second;
    }
    return nullptr;
}


unordered_map<std::string, ServerClient*> *ClientsManager::getClientsByName(){
    return this->clientsMapByName;
}

unordered_map<int, ServerClient*> *ClientsManager::getClientsByFd(){
    return this->clientsMapByFd;
}
