#include "sqlitecontroller.h"
#include "exception.h"
#include <sys/stat.h>
SqliteController::SqliteController()
{
    bool exists = true;
    if(!this->databaseExists("servergg.db")){
        std::cout<<"Database does not exists. Creating new one.\n";
        exists = false;
    }
    int error;
    if((error = sqlite3_open("servergg.db", &db)) != SQLITE_OK){
        throw Exception(error, "Cannot open database.");
    } else {
       std::cout<<"Sqlite: Data base has been opened\n";
       if(!exists){
           char * query = "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    "name varchar(20) not null unique, password varchar(10) not null);";
           char * errmsg;
           int creatingError = sqlite3_exec(getDB(), query, NULL, 0, &errmsg);
           if(creatingError < 0 ){
               throw Exception(creatingError, errmsg);
           } else {
               std::cout<<"Sqlite: User table created successfully!\n";

           }
       }
    }
}

void SqliteController::insertNewUser(std::string &name, std::string &password){
    char * errmsg;
    int error = sqlite3_exec(getDB(), ("insert into users (name, password) values('" + name + "','" + password + "');").c_str(), 0, 0, &errmsg);
    if(error != SQLITE_OK){
        std::string s = std::string(errmsg);

        if(s.find("UNIQUE") != std::string::npos){
                s = std::string("User with this name exists!");
                throw Exception(error, s);
        }
        throw Exception(error, s);
    }
}

bool SqliteController::userExist(std::string &name){
    char * errmsg;
    char ** res;
    int row, col;
    int error = sqlite3_get_table(getDB(), ("select id from users where name='"+name+"';").c_str(), &res, &row, &col, &errmsg);
    if(error != SQLITE_OK){
        std::string s = std::string(errmsg);
        throw Exception(error, s);
    }
    sqlite3_free_table(res);
    return row > 0;
}

bool SqliteController::checkCredentials(std::string &name, std::string &password){
    char * errmsg;
    char ** res;
    int row, col;
    int error = sqlite3_get_table(getDB(), ("select id from users where name='" + name + "' and password='" + password + "';").c_str(), &res, &row, &col, &errmsg);
    if(error != SQLITE_OK){
        std::string s = std::string(errmsg);
        throw Exception(error, s);
    }
    sqlite3_free_table(res);
    return row > 0;
}

std::string SqliteController::getUsersNameByFilterName(std::string name){

    int rows, cols;

    char ** users = getTable("select name from users where name like '%" + name + "%' limit 10;", rows, cols);

    if(users != nullptr){
       std::string result;
       for(int i = 1; i < (rows + 1)* cols; i++){
           result.append("||");
           result.append(std::string(users[i]));
       }
       sqlite3_free_table(users);
       return result;
    }
    return "";
}

char ** SqliteController::getTable(std::string sqlstatement, int & rows, int &cols){
    char * errmsg;// = new char[100];
    char ** res;
    int error = sqlite3_get_table(getDB(), sqlstatement.c_str(), &res, &rows, &cols, &errmsg);

    if(error != SQLITE_OK){
        std::string s = std::string(errmsg);
        throw Exception(error, s);
    }

    if(rows > 0){
        return res;
    }
    return nullptr;
}

SqliteController::~SqliteController(){
    if(db != nullptr){
        sqlite3_close(db);
    }
}

SqliteController& SqliteController::getInstance()
{
    static SqliteController controll;
    return controll;
}

sqlite3* SqliteController::getDB(){
    return this->db;
}

bool SqliteController::databaseExists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}


