#ifndef NEWEPOLL_H
#define NEWEPOLL_H
#include <sys/epoll.h>
#include <errno.h>
#include <exception.h>
class NewEpoll
{
private:
    int epollDesc;

public:
    NewEpoll(int flags);
    int wait(epoll_event * ev, int maxEvents, int timeout);
    void addEvent(int desc, int eventType);
    void removeEvent(int desc);
    int getDescriptor();


};

#endif // NEWEPOLL_H
