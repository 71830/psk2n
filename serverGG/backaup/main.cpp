
#include <iostream>
#include <server.h>
#include <serverexception.h>
#include <cstdio>
#include <stdlib.h>
#include <sqlitecontroller.h>

using namespace std;

int main(int argc, char ** argv)
{

    const char * ip = "127.0.0.1";
    int port = 8000, listenQueueSize = 10;


    if(argc >= 2)
        ip = argv[1];
    if (argc >= 3){
        port = atoi(argv[2]);
        if(argc == 4)
            listenQueueSize = atoi(argv[3]);
    }

    try
    {
        Server * server = new Server(ip, port, listenQueueSize);
        cout<<"Server created! Starting listening on IP: " << server->getSocket()->getIp() << " on PORT: " << server->getSocket()->getPort()<<endl;
        server->run();
    }
    catch(Exception & ex)
    {
        cout<<ex.getErrorMessage()<< " " << ex.getSystemErrorMessage();
    }
    getchar();
    return 0;
}

