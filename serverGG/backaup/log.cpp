#include "log.h"
using namespace std;
Log::Log()
{
}

void Log::i(std::string title, std::string message){
    Log::m("[INFO] " + title, message);
}

void Log::d(std::string title, std::string message){
    Log::m("[DEBUG] " + title, message);
}
void Log::e(std::string title, std::string message){
    Log::m("[ERROR] " + title, message);
}

void Log:: m(std::string title, std::string message){
    cout<<title<< " " << message <<"\n";
}

void Log::m(std::string message){
    Log::m("[MESSAGE] ", message);
}
