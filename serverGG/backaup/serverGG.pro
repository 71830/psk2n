TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += thread
CONFIG += --std=c++11
CONFIG += C++11
LIBS += -lsqlite3

QMAKE_CFLAGS_WARN_ON += -Wall
QMAKE_CXXFLAGS_WARN_ON += -Wall

SOURCES += main.cpp \
    server.cpp \
    exception.cpp \
    serverexception.cpp \
    command.cpp \
    clientsmanager.cpp \
    serverclient.cpp \
    socket.cpp \
    newepoll.cpp \
    sqlitecontroller.cpp \
    commandreader.cpp \
    commandsender.cpp \
    log.cpp

HEADERS += \
    server.h \
    exception.h \
    serverexception.h \
    commandenum.h \
    command.h \
    clientsmanager.h \
    serverclient.h \
    socket.h \
    newepoll.h \
    statics.h \
    sqlitecontroller.h \
    commandreader.h \
    commandsender.h \
    log.h

FORMS +=

