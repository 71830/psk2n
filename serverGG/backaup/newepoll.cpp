#include "newepoll.h"

NewEpoll::NewEpoll(int flags)
{
    this->epollDesc = epoll_create1(flags);

    if(this->epollDesc < 0){
        throw Exception(errno, "Cannot create Epoll");
    }
}

int NewEpoll::wait(epoll_event * ev, int maxEvents, int timeout){
    return epoll_wait(this->epollDesc, ev, maxEvents, timeout);
}

void NewEpoll::addEvent(int desc, int eventType){
    epoll_event event;
    event.data.fd = desc;
    event.events = eventType;

    if(epoll_ctl(this->epollDesc, EPOLL_CTL_ADD, desc, &event) < 0){
        throw Exception(errno, "Cannot add new event to epoll.");
    }
}

void NewEpoll::removeEvent(int desc){
    epoll_ctl(this->getDescriptor(), EPOLL_CTL_DEL, desc, nullptr);
}

int NewEpoll::getDescriptor(){
    return this->epollDesc;
}

