#include "server.h"
#include "serverclient.h"
#include "unistd.h"
#include <sqlitecontroller.h>
#include <commandreader.h>
#include <commandsender.h>
#include <cassert>
#include <unordered_map>
#include <log.h>
#include <stringext.h>

Server::Server(const char * serverIp, int port, int listenQueueSize)
{
    SqliteController::getInstance();


    if(listenQueueSize <= 0){
        throw ServerException(0, "Listen queue size must be greater than 0.");
    }

    try{
        this->socket = new Socket(serverIp, port);
    } catch(Exception & ex){
        throw ServerException(ex.getErrorNumber(), ex.getErrorMessage());

    }

    this->clientsManager = new ClientsManager();
    this->acceptLoopCanRun = true;

}

Server::~Server(){
    delete this->socket;
    delete this->clientsManager;
}

void Server::run()
{
   this->getSocket()->bindSocket();
   this->getSocket()->startListen(this->listenQueueSize);
   this->acceptLoop();
}


void Server::acceptLoop(){ 

    this->createNewEpollAndAddAcceptEvent();

    epoll_event e;

    while(this->acceptLoopCanRun && this->getEventPoll()->wait(&e, 1, -1)){
        if(e.data.fd == this->getSocket()->getDescriptor()){
            this->acceptClient();

        } else if (getClientsManager()->getClientByFd(e.data.fd) != nullptr){
            if(e.events & EPOLLRDHUP){

                ServerClient * client = getClientsManager()->getClientByFd(e.data.fd);

                if(client->isLogged()){
                    std::string userLoggedOut(client->getUsername());
                    this->sendUserLoggedOutMessageToMembers(userLoggedOut);
                    Log::i("Server", "Sending message of logout to users");
                }

                removeClient(getClientsManager()->getClientByFd(e.data.fd));

            }
            else if(e.events & EPOLLIN){
                Command * cmd = CommandReader(e.data.fd).readCommand();
                executeCommand(cmd, e.data.fd);

            }
        } else if( e.data.fd != -1000 && e.events != 1000) {
            assert(false);
        }
        e.data.fd = -1000;
        e.events = 1000;

    }
    this->getSocket()->closeSocket();
}

bool Server::executeCommand(Command *cmd, int fd){
    if(!cmd->validate()) return false;

    switch (cmd->getType()) {
    case CommandType::REGISTER:
        {
            ServerClient* client = getClientsManager()->getClientByFd(fd);
            if(client != nullptr){
                try{

                    std::string name(cmd->getFrom());
                    std::string pass(cmd->getMessage());

                    SqliteController::getInstance().insertNewUser(name, pass);
                    client->setUsername(cmd->getFrom());
                    getClientsManager()->addByUsername(client);

                    Command * servCmd = new Command("Success", CommandType::REGISTER_SUCCESS, "server", cmd->getFrom());
                    CommandSender(client->getSocket()->getDescriptor()).sendCommand(servCmd);


                    Log::i("Server", "User " + name + " has been registered!");

                    delete servCmd;

                } catch (Exception &ex){


                    Command* servCmd = new Command(ex.getErrorMessage(), CommandType::REGISTER_FAILED, "server", cmd->getFrom());
                    CommandSender(client->getSocket()->getDescriptor()).sendCommand(servCmd);
                    Log::e("Server", "Cannot register new user " + ex.getErrorMessage());
                    Log::d("Server", ex.getSystemErrorMessage());

                    delete servCmd;
                }
            }
            break;
        }
    case CommandType::LOG_IN:
        {

            ServerClient* loggedClient = getClientsManager()->getClientByName(std::string(cmd->getFrom()));
            ServerClient* client = getClientsManager()->getClientByFd(fd);

            Command * servCmd;
            bool logged = false;
            if(client != nullptr){

                try{
                    std::string name(cmd->getFrom());
                    std::string pass(cmd->getMessage());

                    if(loggedClient != nullptr && loggedClient->isLogged()){
                        servCmd = new Command("Bad login request user with this username is already logged in.", CommandType::LOGGING_FAILED, "server", cmd->getFrom());
                        Log::e("Server","Bad login request user with this username is already logged in. Error for descriptor: " + std::to_string(client->getSocket()->getDescriptor()) + " with user: " + std::string(cmd->getFrom()));

                    } else if(SqliteController::getInstance().checkCredentials(name, pass)){
                        client->setUsername(std::string(cmd->getFrom()));
                        getClientsManager()->addByUsername(client);

                        servCmd = new Command(getClientsManager()->prepareToSendMembersAndLogged(name), CommandType::LOGGING_SUCCESS, "server", cmd->getFrom());

                        client->setLogged(true);
                        Log::i("Server", "User " + name + " has been logged in!");
                        logged = true;

                    } else {
                        servCmd = new Command("Failed. User does not exists or wrong password.", CommandType::LOGGING_FAILED, "server", cmd->getFrom());
                        Log::e("Server", "Bad login request from descriptor " + std::to_string(client->getSocket()->getDescriptor())  + " with name " + name);

                    }
                } catch (Exception &ex){
                    servCmd = new Command("Failed. Database problem found. Try again.", CommandType::LOGGING_FAILED, "server", cmd->getFrom());
                    Log::e("Server", "Database bad login request from descriptor: " + std::to_string(client->getSocket()->getDescriptor()) + " with user: " + std::string(cmd->getFrom()));
                }
                CommandSender(client->getSocket()->getDescriptor()).sendCommand(servCmd);
                if(logged){
                    std::string userNameForMembers(cmd->getFrom());
                    sendUserLoggedMessageToMembers(userNameForMembers);
                    Log::i("Server", "Sending message to members that user logged.");

                    std::string to(servCmd->getTo());
                    sendUserNotSentMessages(to, client->getSocket()->getDescriptor());
                }
                delete servCmd;
            }
            break;
        }
    case CommandType::SEND_MESSAGE:
        {
            ServerClient* client = getClientsManager()->getClientByName(cmd->getTo());
            bool sent = false;
            if(client != nullptr){
              sent = (CommandSender(client->getSocket()->getDescriptor()).sendCommand(cmd) < 0);
            }
            int i = sent ? 1 : 0;

            std::string from(cmd->getFrom());
            std::string to(cmd->getTo());
            std::string msg(cmd->getMessage());

            SqliteController::getInstance().addMessage(from, to, msg, i);
            Log::m("Sending from " +  std::string(cmd->getFrom()) + " to " + std::string(cmd->getTo()));

            break;
        }
    case CommandType::USERS_SEARCH:
        {
            std::string name(cmd->getMessage());
            std::string users = SqliteController::getInstance().getUsersNameByFilterName(name);
            CommandType cmdType = CommandType::USERS_SEARCH;
            if(users.empty()){ users = std::string("No users found"); cmdType = CommandType::USERS_SEARCH_NOT_FOUND; }
            Command * sendUsersCmd = new Command(users, cmdType, "Server", cmd->getFrom());
            CommandSender(fd).sendCommand(sendUsersCmd);

            delete sendUsersCmd;
            Log::i("Server", "Sending searched users to " + std::string(cmd->getFrom()));
            break;
        }
     case CommandType::ADD_FRIEND:
        {
            std::string from(cmd->getFrom());
            std::string to(cmd->getMessage());
            bool added = SqliteController::getInstance().addUserMember(from, to);

            ServerClient * clientTo = getClientsManager()->getClientByName(to);
            bool logged = false;
            if(clientTo != nullptr){
                logged = clientTo->isLogged();
            }
            int loggedInt = logged ? 1 : 0;
            CommandType tp = added ? FRIEND_ADDED : FRIEND_NOT_ADDED;
            std::string message = added ? "||" + to + "||" + to_string(loggedInt) : "Cannot add friend on the server. Try again.";

            Command * sendUserResponse = new Command(message, tp, "Server", cmd->getFrom());
            CommandSender(fd).sendCommand(sendUserResponse);

            Log::i("Server", "Sending response if friend added to " + std::string(cmd->getFrom()));
            delete sendUserResponse;
            break;
        }
    case CommandType::REMOVE_FRIEND:
       {
           std::string from(cmd->getFrom());
           std::string to(cmd->getMessage());
           bool removed = SqliteController::getInstance().removeUserMember(from, to);
           Command * sendUserResponse;
           if(removed){
             sendUserResponse = new Command(cmd->getMessage(), CommandType::FRIEND_REMOVED, "Server", cmd->getFrom());
           } else {
             sendUserResponse = new Command(cmd->getMessage(), CommandType::FRIEND_NOT_REMOVED, "Server", cmd->getFrom());
           }
           CommandSender(fd).sendCommand(sendUserResponse);

           Log::i("Server", "Sending response if friend removed to " + std::string(cmd->getFrom()));
           delete sendUserResponse;
           break;
       }


    default:
        break;
    }
    delete cmd;
    return true;
}

void Server::acceptClient(){
    sockaddr_in clientSocket;
    socklen_t socketSize;

    int acceptValue = accept(this->getSocket()->getDescriptor(), (sockaddr*)&clientSocket, &socketSize);
    if(acceptValue > 0){
        this->addNewClient(acceptValue, &clientSocket);
    }
}

void Server::createNewEpollAndAddAcceptEvent(){
    try{
        this->eventPoll = new NewEpoll(0);
        this->getEventPoll()->addEvent(this->getSocket()->getDescriptor(), EPOLLIN);
    } catch (Exception & ex){
        throw ServerException(ex.getErrorNumber(), ex.getErrorMessage());
    }
}

void Server::addNewClient(int acceptValue, sockaddr_in * sockAddr){
    try{
       this->getEventPoll()->addEvent(acceptValue, EPOLLIN | EPOLLRDHUP);
       try{
            ServerClient * client = new ServerClient(new Socket(acceptValue, *sockAddr));

            this->getClientsManager()->addByFd(client);

        }catch(Exception & ex){
            this->getEventPoll()->removeEvent(acceptValue);
            ex.printError();
        }
    } catch (Exception  &ex){
        ex.printError();
    }
}

void Server::removeClient(ServerClient * client){
    this->getEventPoll()->removeEvent(client->getSocket()->getDescriptor());
    this->getClientsManager()->remove(client);
    Log::i("Server", "Client " + client->getUsername() + " has been removed.");
}

Socket * Server::getSocket(){
    return this->socket;
}


ClientsManager * Server::getClientsManager(){
    return this->clientsManager;
}

NewEpoll * Server::getEventPoll(){
    return this->eventPoll;
}

void Server::sendMessageToClientsFromList(std::vector<ServerClient*> clients, Command * cmd){
    for(std::vector<ServerClient*>::iterator it = clients.begin(); it != clients.end(); it++){
        CommandSender((*it)->getSocket()->getDescriptor()).sendCommand(cmd);
    }
}

void Server::sendUserLoggedMessageToMembers(std::string &user){
    Command * cmd = new Command(user, CommandType::USER_LOGGED, "Server", "member");
    sendMessageToClientsFromList(getClientsManager()->getAllLoggedUsersFromList(SqliteController::getInstance().getUsersThatUserMember(user)), cmd);
    delete cmd;
}

void Server::sendUserLoggedOutMessageToMembers(std::string &user){
    Command * cmd = new Command(user, CommandType::USER_LOGGED_OUT, "Server", "member");
    sendMessageToClientsFromList(getClientsManager()->getAllLoggedUsersFromList(SqliteController::getInstance().getUsersThatUserMember(user)), cmd);
    delete cmd;
}

void Server::sendUserNotSentMessages(std::string &userTo, int sdesc){
    std::vector<std::string> results = SqliteController::getInstance().getMessages(userTo, 0);

    Log::i("Server", "Sending not sent messages. Count: " + std::to_string(results.size()));

    for(unsigned int i = 0; i < results.size(); i++){
          std::vector<std::string> row = StringExt::split(results[i].c_str(), "||");
          if(row.size() > 3){
              Command * cmd = new Command(row[3], CommandType::SEND_MESSAGE, row[1], row[2]);

              if(CommandSender(sdesc).sendCommand(cmd) < 0){
                  delete cmd;
                  break;
              } else {
                  SqliteController::getInstance().setMessageSent(atoi(row[0].c_str()), 1);
              }
              delete cmd;
          }
   }
}

