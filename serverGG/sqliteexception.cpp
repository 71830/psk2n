#include "sqliteexception.h"

SqliteException::SqliteException(int errorNumber, std::string message) : Exception(errorNumber, message){

}

std::string SqliteException::getSystemErrorMessage(){
    return sqlite3_errstr(this->getErrorNumber());
}

