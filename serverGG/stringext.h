#ifndef STRINGEXT_H
#define STRINGEXT_H
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cstring>

class StringExt
{
public:

    /** przykładowa implementacja: **/
    static std::vector < std::string > split(const char * napis, const char * ograniczniki = "\n\t " ) {
        std::vector < std::string > podzielony_napis;

        char * n = new char[strlen(napis) + 1];
        strcpy(n, napis);

        for( char * pch = strtok( n, ograniczniki ); pch != NULL; pch = strtok( NULL, ograniczniki ) )
             podzielony_napis.push_back( pch );

        delete [] n;
        return podzielony_napis;
    }
};

#endif // STRINGEXT_H
