#include "sssqlitecontroller.h"
#include <exception.h>
#include <sys/stat.h>



SSSqliteController::SSSqliteController(std::string & name, std::string & createQuery)
{
    bool exists = true;
    if(!this->databaseExists(name)){
        std::cout<<"Database does not exists. Creating new one.\n";
        exists = false;
    }

    int error;
    if((error = sqlite3_open(name.c_str(), &db)) != SQLITE_OK){
        throw Exception(error, "Cannot open database.");
    } else {
       std::cout<<"Sqlite: Data base has been opened\n";
       if(!exists){
           char * errmsg;
           int creatingError = sqlite3_exec(getDB(), createQuery.c_str(), NULL, 0, &errmsg);
           if(creatingError < 0 ){
               throw Exception(creatingError, errmsg);
           } else {
               std::cout<<"Sqlite: Tables created successfully!\n";
           }
       }
    }
}

sqlite3* SSSqliteController::getDB(){
    return this->db;
}

bool SSSqliteController::databaseExists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

SSSqliteController::~SSSqliteController(){
    if(db != nullptr){
        sqlite3_close(db);
    }
}
