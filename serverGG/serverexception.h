#ifndef SERVEREXCEPTION_H
#define SERVEREXCEPTION_H
#include <exception.h>
class ServerException : public Exception
{    
public:
    ServerException();
    ServerException(int errorNumber, std::string errorMessage);
};

#endif // SERVEREXCEPTION_H
