#ifndef EXCEPTION_H
#define EXCEPTION_H

#include<string.h>
#include<string>
#include <iostream>
class Exception
{
private:
    int errorNumber;
    std::string errorMessage;

public:
    Exception();
    Exception(int errorNumber, std::string errorMessage);

    int virtual getErrorNumber();
    std::string virtual getErrorMessage();
    std::string virtual getSystemErrorMessage();

    void printError(){ std::cout<< "[LOG][ERROR]: "<< getErrorMessage() << "\n"; }

};



#endif // EXCEPTION_H
