#ifndef STATICS
#define STATICS

constexpr int CommandTypeSize = 2;
constexpr int MessageBufSize = 4;
constexpr int UserNameBufSize = 21;

#endif // STATICS

