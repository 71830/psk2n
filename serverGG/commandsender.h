#ifndef COMMANDSENDER_H
#define COMMANDSENDER_H
#include "command.h"


class CommandSender
{
public:
    CommandSender(int fd);
    int sendCommand(Command * cmd);
private:
    int fd;
};

#endif // COMMANDSENDER_H
