#include "clientsqlitecontroller.h"
#include <QStringList>
ClientSqliteController::ClientSqliteController(std::string & name, std::string & createQuery) : SSSqliteController(name, createQuery)
{
}

ClientSqliteController &  ClientSqliteController::getInstance(std::string name){
    static std::string createQuery ="CREATE TABLE members (id integer primary key autoincrement, name varchar(20) not null unique);"
            "CREATE TABLE messages(userfromname varchar(20) not null, usertoname varchar(20) not null, message varchar(300), timestamp datetime default current_timestamp, primary key(userfromname, usertoname, timestamp));";

    static ClientSqliteController controller(name, createQuery);
    return controller;
}

bool ClientSqliteController::addMessage(std::string &msgFrom, std::string &msgTo, std::string &msg){
    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "insert into messages (userfromname, usertoname, message) values (?, ?, ?);", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, msgFrom.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, msgTo.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 3, msg.c_str(), -1, SQLITE_STATIC);


    if(result != SQLITE_OK){
        return false;
    }

    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return result == SQLITE_DONE;
}

bool ClientSqliteController::addMember(std::string & name){
    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "insert into members (name) values (?);", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);

    if(result != SQLITE_OK){
        return false;
    }

    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return result == SQLITE_DONE;
}

bool ClientSqliteController::removeMember(std::string & name){
    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "delete from members where name = ?;", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);

    if(result != SQLITE_OK){
        return false;
    }

    result = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return result == SQLITE_DONE;
}

bool ClientSqliteController::memberExists(std::string & name){
    sqlite3_stmt * stmt;
    int result = sqlite3_prepare_v2(getDB(), "select count(*) from members where name = ?", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, name.c_str(), -1, SQLITE_STATIC);

    if(result != SQLITE_OK){
        return false;
    }

    result = sqlite3_step(stmt);

    int added = 0;
    while(result == SQLITE_ROW){
        added =  sqlite3_column_int(stmt, 0);
        result = sqlite3_step(stmt);
    }

    sqlite3_finalize(stmt);

    return result == SQLITE_DONE && added > 0;
}

void ClientSqliteController::checkServerMembersSynchronized(QString members){
    QStringList list = members.split("||", QString::SplitBehavior::SkipEmptyParts);
    std::string user;
    for(int i = 0; i < list.size(); i++){
       user =list[i].split("&", QString::SplitBehavior::KeepEmptyParts)[0].toStdString();
       if(!this->memberExists(user)){
           this->addMember(user);
       }
    }
}

std::vector<std::string> ClientSqliteController::getUnknownMembers(std::string ownername){
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(getDB(), "select distinct case when mes.usertoname = ? then mes.userfromname else usertoname end from messages mes except select m.name from members m;", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, ownername.c_str(),-1, SQLITE_STATIC);

    std::vector<std::string> list;

    if(result != SQLITE_OK){
        sqlite3_finalize(stmt);
        return list;
    }

    result = sqlite3_step(stmt);

    while(result == SQLITE_ROW){
        list.push_back(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));
        result = sqlite3_step(stmt);
    }

    sqlite3_finalize(stmt);
    return list;
}

std::vector<std::string> ClientSqliteController::getMembers(){
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(getDB(), "select name from members order by name asc;", -1, &stmt, 0);
    std::vector<std::string> list;

    if(result != SQLITE_OK){
        sqlite3_finalize(stmt);
        return list;
    }

    result = sqlite3_step(stmt);

    while(result == SQLITE_ROW){
        list.push_back(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));
        result = sqlite3_step(stmt);
    }

    sqlite3_finalize(stmt);
    return list;
}

std::vector<std::vector<std::string>> ClientSqliteController::getMessages(std::string &toname){
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare_v2(getDB(), "select message, userfromname, usertoname, timestamp from messages where userfromname = ? or usertoname = ? order by timestamp asc;", -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, toname.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, toname.c_str(), -1, SQLITE_STATIC);

    std::vector<std::vector<std::string>> results;

    if(result != SQLITE_OK){
        sqlite3_finalize(stmt);
        return results;
    }

    result = sqlite3_step(stmt);

    while(result == SQLITE_ROW){
        std::vector<std::string> vec;
        for(int i = 0; i < 4; i++){
            std::string s = reinterpret_cast<const char *>(sqlite3_column_text(stmt, i));
            vec.push_back(s);
        }
        results.push_back(vec);
        result = sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);

    return results;

}
