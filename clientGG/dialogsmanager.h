#ifndef DIALOGSMANAGER_H
#define DIALOGSMANAGER_H
#include <unordered_map>
#include <chatbox.h>
#include <string>
class DialogsManager
{
    typedef std::unordered_map<std::string, ChatBox*> DialogMap;
public:
    DialogsManager();
    ~DialogsManager(){
        for(DialogMap::iterator it= getDialogs().begin(); it != getDialogs().end(); ++it){
            delete it->second;
        }
    }


    bool chatBoxExists(std::string username);
    ChatBox * getChatBox(std::string username);
    void addChatBox(std::string, ChatBox * chatbox);
    int removeChatBox(std::string username);
    int getCount();
    void checkIfDialogExistsAndAddMessageFromSender(const char * message, std::string username, std::string owner);
    void createDialogAndAddMessageFromSender(const char * message, std::string username, std::string owner);
    void createAndAddNewChatBox(std::string username, std::string owner);
private:
    DialogMap dialogs;
    DialogMap & getDialogs();
};

#endif // DIALOGSMANAGER_H
