#ifndef CUSTOMFRIENDLISTWIDGETITEM_H
#define CUSTOMFRIENDLISTWIDGETITEM_H

#include <QListWidgetItem>
#include <QLabel>
#include <QToolButton>

class CustomFriendListWidgetItem : public QListWidgetItem
{

public:
    explicit CustomFriendListWidgetItem(QString labelText, bool logged, QListWidget *parent);
    ~CustomFriendListWidgetItem(){
        //delete label;
        //delete removeButton;
        //delete widget;
        //delete icon;
        //delete iconLabel;
    }

    QToolButton * getRemoveButton() { return this->removeButton; }
    void setLogged(bool logged);
private:
    QLabel * label;
    QToolButton * removeButton;
    QWidget * widget;
    QIcon * icon;
    QLabel * iconLabel;

};

#endif // CUSTOMFRIENDLISTWIDGETITEM_H
