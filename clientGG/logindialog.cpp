#include "logindialog.h"
#include "ui_logindialog.h"
#include <QMessageBox>
#include "mainwindow.h"
#include "command.h"
#include "commandreader.h"
#include "commandsender.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);

    QRegExp re("[0-9a-zA-Z]+");

    QRegExpValidator * validator = new QRegExpValidator(re, this);
    this->ui->passwordLineEdit->setValidator(validator);
    this->ui->usernameLineEdit->setValidator(validator);

    this->dialogResultValue = NONE;
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_registerButton_clicked()
{
    this->dialogResultValue = REGISTER;
    this->accept();

}

void LoginDialog::on_loggingButton_clicked()
{
    login();
}

void LoginDialog::login(){
    if(this->validate()){
        Socket socket = MainWindow::GetSocket();

        if(socket.isConnected()){
            this->loginCommand(socket.getDescriptor());
        } else {
            try{
                socket.connectSocket();
                this->loginCommand(socket.getDescriptor());
            } catch (Exception & ex){
               int result = QMessageBox::warning(this, "Connecting error", QString::fromStdString(ex.getErrorMessage()), QMessageBox::Retry, QMessageBox::Cancel);
               if(result == QMessageBox::Retry){
                   this->login();
               }
            }
        }
    }
}

void LoginDialog::loginCommand(int fd){



    Command * cmd = new Command(this->credentials.getPassword() ,CommandType::LOG_IN, this->credentials.getUsername(), "Server");
    CommandSender(fd).sendCommand(cmd);
    Command * commandLoginResult = CommandReader(fd).readCommand();

    if(commandLoginResult->getType() == CommandType::LOGGING_SUCCESS){
        this->dialogResultValue = LoginDialog::DialogResult::LOGGING_SUCCESS;
        this->loggedMembersResponse = QString(commandLoginResult->getMessage());

        this->accept();
    } else if(commandLoginResult->getType() == CommandType::LOGGING_FAILED) {
        int result = QMessageBox::warning(this, "Logging error", commandLoginResult->getMessage(), QMessageBox::Retry, QMessageBox::Cancel);
        if(result == QMessageBox::Retry){
            this->login();
        }
    }
    delete cmd;
    delete commandLoginResult;
}

bool LoginDialog::validate(){
    QMessageBox message;
    message.setWindowTitle("Logging error");
    if(ui->usernameLineEdit->text().isEmpty()){
        message.setText("Username cannot be empty!");
        message.exec();
    } else if (ui->passwordLineEdit->text().isEmpty()){
        message.setText("Password cannot be empty!");
        message.exec();
    } else {
        this->credentials = Credentials(this->ui->usernameLineEdit->text().toStdString(), this->ui->passwordLineEdit->text().toStdString());
        return true;
    }
    return false;
}


LoginDialog::DialogResult LoginDialog::getDialogResult(){
    return this->dialogResultValue;
}

std::string LoginDialog::getUsername(){
    return this->credentials.getUsername();
}
