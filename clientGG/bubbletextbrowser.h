#ifndef BUBBLETEXTBROWSER_H
#define BUBBLETEXTBROWSER_H

#include <QTextBrowser>

class BubbleTextBrowser : public QTextBrowser
{
    Q_OBJECT
public:
    explicit BubbleTextBrowser(QWidget *parent = 0):QTextBrowser(parent){}
    explicit BubbleTextBrowser(QColor color);
    ~BubbleTextBrowser();

signals:

public slots:
    void OnDocumentSizeChanged(const QSizeF & newSize);

};

#endif // BUBBLETEXTBROWSER_H
