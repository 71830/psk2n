#include "commandreader.h"
#include<iostream>
#include "statics.h"
#include <sys/unistd.h>
#include <exception.h>

CommandReader::CommandReader(int fd)
{
    this->fd = fd;
    this->error = 0;
}


char * CommandReader::readUserName(int filedescriptor){
    char * user = new char[UserNameBufSize+1];
    user[UserNameBufSize] = '\0';
    int readc = read(filedescriptor, user, UserNameBufSize);
    if(readc != UserNameBufSize)
        return nullptr;
    return user;
}

CommandType CommandReader::readCommandType(int filedescriptor){

    char commandTypeBuf[CommandTypeSize];

    int readc = read(filedescriptor, commandTypeBuf, CommandTypeSize);
    return readc != CommandTypeSize ? (CommandType)(-1) : static_cast<CommandType>(atoi(commandTypeBuf));
}


char * CommandReader::readMessage(int filedescriptor, int sizeOfFirstRead){

    char temp[sizeOfFirstRead];
    int readc = read(filedescriptor, temp, sizeOfFirstRead);
    if(readc > 0){
        int size = atoi(temp);

        char * buffer = new char[size+1];
        buffer[size] = '\0';
        readc = read(filedescriptor, buffer, size);
        if(readc > 0){
            return buffer;
        }
    }
    return nullptr;

}

Command *CommandReader::readCommand(int filedescriptor){
    CommandType commandType = readCommandType(filedescriptor);
    char * messageBuf = readMessage(filedescriptor, MessageBufSize);

    char * from = readUserName(filedescriptor);
    char * to = readUserName(filedescriptor);

    if(messageBuf == nullptr || from == nullptr || to == nullptr){
        this->error = errno;
        this->errmsg = strerror(this->error);
    }

    if(messageBuf == nullptr){
        messageBuf = new char[2];
        messageBuf[0] = ' ';
        messageBuf[1] = '\0';
    }

    if(from == nullptr){
        from = new char[2];
        from[0] = ' ';
        from[1] = '\0';
    }


    if(to == nullptr){
        to = new char[2];
        to[0] = ' ';
        to[1] = '\0';
    }





    Command * command =  new Command(messageBuf, commandType, from, to);

    delete messageBuf;
    delete from;
    delete to;

    return command;
}

Command * CommandReader::readCommand(){
    return readCommand(this->fd);
}

int CommandReader::getError(){
    return this->error;
}

char * CommandReader::getErrorMessage(){
    return this->errmsg;
}
