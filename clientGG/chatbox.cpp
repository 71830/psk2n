#include "chatbox.h"
#include "ui_chatbox.h"
#include <QtGui>
#include <QThread>
#include <QApplication>
#include <cmdthreadsender.h>
#include <QMessageBox>
#include <mainwindow.h>
#include <QTextBrowser>
#include <QScrollBar>
#include <bubbletextbrowser.h>
#include <clientsqlitecontroller.h>
ChatBox::ChatBox(std::string username, std::string owner, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChatBox)
{
    ui->setupUi(this);
    setWindowTitle(QString::fromStdString(username));
    ///
    this->scrollLayout = new QVBoxLayout(this->ui->scrollArea->widget());

    QScrollBar* scrollbar = this->ui->scrollArea->verticalScrollBar();
    QObject::connect(scrollbar, SIGNAL(rangeChanged(int,int)), this, SLOT(moveScrollBarToBottom(int, int)));
    ///

    this->username = username;
    this->owner = owner;

    this->ui->boldButton->setCheckable(true);
    this->ui->boldButton->setStyleSheet("QPushButton:checked { background-color: red; }");

    this->ui->italicButton->setCheckable(true);
    this->ui->italicButton->setStyleSheet("QPushButton:checked { background-color: red; }");

    this->ui->underLineButton->setCheckable(true);
    this->ui->underLineButton->setStyleSheet("QPushButton:checked { background-color: red; }");

    this->setWindowTitle(QString(this->username.c_str()));

    qDebug() << "executing thread id - " << QThread::currentThreadId();
}

ChatBox::~ChatBox()
{
    delete ui;
    delete scrollLayout;
}

void ChatBox::on_sendMessageButton_clicked()
{



    if(!this->ui->writeMessageAreaTextEdit->toPlainText().isEmpty()){
        QString formatted = this->formatTextToSend();

        Command *cmd = new Command(formatted.toStdString(), CommandType::SEND_MESSAGE, this->owner, this->username);
        this->addNewMessageFromYouToScrollArea(formatted);


        std::string message(formatted.toStdString());
        ClientSqliteController::getInstance(this->owner).addMessage(this->owner, this->username, message);


        CmdThreadSender *  thread = new CmdThreadSender(cmd,  MainWindow::GetSocket().getDescriptor());
        connect(thread, &CmdThreadSender::resultReady, this, &ChatBox::OnMessageSent);

        thread->start();


    }
    this->ui->writeMessageAreaTextEdit->setFocus();
}


void ChatBox::OnMessageSent(int error, const char * errmsg, Command * cmd){
    qDebug() << "On Message sent thread id " << QThread::currentThreadId();
    if(error < 0)
        QMessageBox::information(this, "Sending message error " + QString(cmd->getTo()), QString("Error occurred when sending message. ") + QString(errmsg), QMessageBox::StandardButton::Ok, 0);
    QObject * s = sender();
    delete s;
}

QString ChatBox::formatTextToSend(){
    QString string = this->ui->writeMessageAreaTextEdit->toPlainText();
    if(bold)
        string = "<b>" + string + "</b>";
    else
    {
        string.replace("<b>", "");
        string.replace("</b>", "");
    }

    if(italic)
        string = "<i>" + string + "</i>";
    else
    {
        string.replace("<i>", "");
        string.replace("</i>", "");
    }
    if(underline)
        string = "<u>" + string + "</u>";
    else
    {
        string.replace("<u>", "");
        string.replace("</u>", "");
    }
    return string;
}

void ChatBox::addNewMessageFromYouToScrollArea(QString formatted){

    BubbleTextBrowser * textBrowser = new BubbleTextBrowser(QColor::fromRgb(230, 100, 50));
    textBrowser->setText(formatted);
    textBrowser->setToolTip(this->owner.c_str());



    this->addDateLabel();
    this->scrollLayout->addWidget(textBrowser, 0,  Qt::AlignLeft);


    this->ui->scrollArea->verticalScrollBar()->setValue(this->ui->scrollArea->verticalScrollBar()->maximum());
    this->ui->writeMessageAreaTextEdit->clear();

}

void ChatBox::addMessageFromSenderToScrollArea(const char * message){
    BubbleTextBrowser * textBrowser = new BubbleTextBrowser(QColor::fromRgb(100, 180, 50));
    textBrowser->setText(QString(message));
    textBrowser->setToolTip(this->username.c_str());

    this->addDateLabel();
    this->scrollLayout->addWidget(textBrowser, 0, Qt::AlignRight);
    this->ui->scrollArea->verticalScrollBar()->setValue(this->ui->scrollArea->verticalScrollBar()->maximum());
}

void ChatBox::addDateLabel(){
    QLabel * label = new QLabel(QDateTime::currentDateTime().toString("d.M.yyyy hh:mm:ss"));

    QFont f("Arial", 10);
    label->setFont(f);

    this->scrollLayout->addWidget(label,0, Qt::AlignCenter);
}

void ChatBox::on_writeMessageAreaTextEdit_textChanged()
{
    int maxLength = 160;
    this->ui->charsLeftLabel->setText("Chars left: " +  QString::number(maxLength - this->ui->writeMessageAreaTextEdit->toPlainText().length()));

    if(this->ui->writeMessageAreaTextEdit->toPlainText().length() > maxLength){
        this->ui->writeMessageAreaTextEdit->textCursor().deletePreviousChar();
    }
}

void ChatBox::moveScrollBarToBottom(int min, int max)
{
    Q_UNUSED(min);
    this->ui->scrollArea->verticalScrollBar()->setValue(max);
}

void ChatBox::on_boldButton_clicked()
{
    bold = !bold;
    this->ui->boldButton->setChecked(bold);
}

void ChatBox::on_italicButton_clicked()
{
    italic = !italic;
    this->ui->italicButton->setChecked(italic);
}

void ChatBox::on_underLineButton_clicked()
{
    underline = !underline;
    this->ui->underLineButton->setChecked(underline);
}
