#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <socket.h>
#include <isender.h>
#include <mutex>
#include <cmdthreadreceiver.h>
#include <chatbox.h>
#include <dialogsmanager.h>
#include <membersmanager.h>
#include <customfriendlistwidgetitem.h>
#include <archive.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void login();

    static Socket & GetSocket();
    std::string & getOwnerName();
private slots:
    void OnCommandReceive(int error, char * errmsg, Command * cmd);


    void on_addFriendAction_triggered();

    void on_friendsAction_triggered();

    void on_searchFriendPushButton_clicked();

    void OnCommandSent(int error, const char * errmsg, Command * cmd);

    void OnAddFriendToolButtonClicked();
    void OnRemoveFriendToolButtonClicked();

    void OnMemberSetLogged(std::string username, bool logged);
    void OnMemberAddedToMembersManager(std::string username, bool logged);
    void OnMemberRemovedFromMembersManager(std::string username);

    void OnFriendListItemDoubleClicked(QListWidgetItem * item);


    void on_archiveIcon_triggered();

private:
    static Socket socket;
    Ui::MainWindow *ui;

    std::string username;

    std::mutex sendCommandMutex;

    CmdThreadReceiver * receiver = NULL;

    void connectAndStartReceiver();

    DialogsManager dialogsManager;

    DialogsManager & getDialogsManager();

    MembersManager * membersManager = NULL;

    Archive * archive = NULL;


    void initiateMembersManagerAndFriendsWidget(QString membersListFromLogging);
    void addMembersToFriendsListWidget();

    void setWidgetVisibleAndEnabled(QWidget * widget, bool value);
    void setWidgetVisibleAndEnabled(QWidget * widget, bool visible, bool enabled);
    void setSearchGuiVisibleAndEnabled(bool value);
    void setFriendsGuiVisibleAndEnabled(bool value);

    void addSearchedUsers(const char * message);

    void connectToDataBase();
    void addFriend(Command * cmd);
    void removeFriend(Command * cmd);



    void createSendingThreadAndSendCommand(Command * cmd);

};

#endif // MAINWINDOW_H
