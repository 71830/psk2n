#include "credentials.h"

Credentials::Credentials(std::string username, std::string password)
{
    this->username = username;
    this->password = password;
}

std::string Credentials::getUsername(){
    return this->username;
}

std::string Credentials::getPassword(){
    return this->password;
}
