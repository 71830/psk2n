#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include "credentials.h"
namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT


public:
    enum DialogResult { NONE, LOGGING_SUCCESS, LOGGING_FAILED, REGISTER};
    DialogResult getDialogResult();
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();
    std::string getUsername();
    QString getLoggedMembersResponse() { return this->loggedMembersResponse; }
private slots:
    void on_registerButton_clicked();

    void on_loggingButton_clicked();

private:
    Ui::LoginDialog *ui;
    DialogResult dialogResultValue;
    bool validate();
    Credentials credentials;
    void login();
    void loginCommand(int fd);
    QString loggedMembersResponse;
};

#endif // LOGINDIALOG_H
