#include "archive.h"
#include "ui_archive.h"
#include "bubbletextbrowser.h"
#include <clientsqlitecontroller.h>
#include <QLabel>
#include <QDateTime>
#include <QScrollBar>

Archive::Archive(std::string name, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Archive)
{
    ui->setupUi(this);


    this->scrollLayout = new QVBoxLayout(this->ui->scrollArea->widget());

    this->ownername = name;
    this->ui->treeWidget->setHeaderLabel("Members");



    connect(this->ui->treeWidget, &QTreeWidget::itemClicked, this, &Archive::OnMemberWidgetItemClicked);
}

Archive::~Archive()
{
    delete ui;
    if(scrollLayout != NULL){
        delete scrollLayout;
    }

}

void Archive::show(){

    this->ui->treeWidget->clear();
    clearChildOfScrollLayout();

    std::vector<std::string> memberList = ClientSqliteController::getInstance(this->ownername).getMembers();
    std::vector<std::string> unknownList = ClientSqliteController::getInstance(this->ownername).getUnknownMembers(this->ownername);

    addMemberListToTreeWidget(memberList, true);
    addMemberListToTreeWidget(unknownList, false);

    QWidget::show();

}

void Archive::addMemberListToTreeWidget(std::vector<std::string> l, bool known){
    for(unsigned int i = 0; i < l.size(); i++){
        QTreeWidgetItem * item = new QTreeWidgetItem(this->ui->treeWidget, QStringList(QString(l[i].c_str())));
        if(!known){
            item->setTextColor(0, QColor::fromRgb(100,50,50));
        }
    }
}


void Archive::addFromOwner(QString formatted){

    BubbleTextBrowser * textBrowser = new BubbleTextBrowser(QColor::fromRgb(230, 100, 50));
    textBrowser->setText(formatted);
    textBrowser->setToolTip(this->ownername.c_str());


    this->scrollLayout->addWidget(textBrowser, 0,  Qt::AlignLeft);

    this->ui->scrollArea->verticalScrollBar()->setValue(this->ui->scrollArea->verticalScrollBar()->maximum());
}

void Archive::addFromSender(QString  message, std::string sender){

    BubbleTextBrowser * textBrowser = new BubbleTextBrowser(QColor::fromRgb(100, 180, 50));
    textBrowser->setText(message);
    textBrowser->setToolTip(sender.c_str());

    this->scrollLayout->addWidget(textBrowser, 0, Qt::AlignRight);
    this->ui->scrollArea->verticalScrollBar()->setValue(this->ui->scrollArea->verticalScrollBar()->maximum());
}

void Archive::addDateLabel(QString str){
    QLabel * label = new QLabel(str);

    QFont f("Arial", 10);
    label->setFont(f);

    this->scrollLayout->addWidget(label,0, Qt::AlignCenter);
}



void Archive::OnMemberWidgetItemClicked(QTreeWidgetItem * item, int column){

    std::string s(item->text(column).toStdString());
    std::vector<std::vector<std::string>> resultList = ClientSqliteController::getInstance(this->ownername).getMessages(s);

    clearChildOfScrollLayout();

    for(unsigned int i = 0; i < resultList.size(); i++){
        this->addDateLabel(QString(resultList[i][3].c_str()));
        if(resultList[i][1].compare(this->ownername) == 0){
            this->addFromOwner(resultList[i][0].c_str());
        } else {
            this->addFromSender(resultList[i][0].c_str(), s);
        }
    }

}

void Archive::clearChildOfScrollLayout(){
    if(scrollLayout != NULL){
        QLayoutItem * child;
        while((child = scrollLayout->takeAt(0)) != 0){
            delete child->widget();
        }
    }
}
