#ifndef MEMBERSMANAGER_H
#define MEMBERSMANAGER_H
#include <set>
#include <string>
#include <QString>
#include <QObject>

class MembersManager: public QObject
{
       Q_OBJECT
public:

    MembersManager(){}
    MembersManager(QObject * parent = 0) : QObject(parent){;}
    MembersManager(QString membersAndLogged, QObject * parent = 0);
    virtual ~MembersManager(){;}



    void addMember(std::string username, bool logged);
    void removeMember(std::string username);

    void setLogged(std::string username, bool logged);


    std::set<std::string> &getLoggedMembers(){ return this->loggedMembers; }
    std::set<std::string> &getNotLoggedMembers(){ return this->notLoggedMembers; }

signals:
    void userAdded(std::string username, bool logged);
    void userRemoved(std::string username);
    void userLoggedChanged(std::string username, bool logged);


private:
    std::set<std::string> loggedMembers;
    std::set<std::string> notLoggedMembers;
};

#endif // MEMBERSMANAGER_H
