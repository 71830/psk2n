#ifndef ISENDER_H
#define ISENDER_H
#include <command.h>
#include <QWidget>
class ISender
{
public:
    void send(Command &cmd, QWidget * sender){
        int error = sendCommand(cmd, sender);
        OnCommandSent(cmd, error, sender);
    }
protected:
    virtual int sendCommand(Command &cmd, QWidget * sender) = 0;
    virtual void OnCommandSent(Command & cmd, int error, QWidget * sender) = 0;
};

#endif // ISENDER_H
