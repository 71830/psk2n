#ifndef CMDTHREADRECEIVER_H
#define CMDTHREADRECEIVER_H
#include <QThread>
#include <command.h>
#include <commandreader.h>
#include <QtGui>
class CmdThreadReceiver : public QThread
{
    Q_OBJECT

    void run() Q_DECL_OVERRIDE {
        while(loop){
            qDebug() << "Receiver thread id " << QThread::currentThreadId();
            CommandReader reader(fd);
            Command * cmd = reader.readCommand();
            emit resultReady(reader.getError(), reader.getErrorMessage(), cmd);

            if(reader.getError() != 0) loop = false;
        }
   }


public:
    CmdThreadReceiver(int fd);

    void stop(){
       loop = false;
       this->terminate();
       this->quit();
    }


signals:
    void resultReady(int error, char * errmsg, Command * cmd);

private:
    int fd;
    bool loop;
};

#endif // CMDTHREADRECEIVER_H
