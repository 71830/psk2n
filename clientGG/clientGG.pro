#-------------------------------------------------
#
# Project created by QtCreator 2015-12-28T14:50:29
#
#-------------------------------------------------




QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = clientGG
TEMPLATE = app

QMAKE_CXXFLAGS_WARN_ON += -Wall -Wextra -Werror

CONFIG += --std=c++11
CONFIG += C++11
LIBS += -lexplain
LIBS += -lsqlite3
SOURCES += main.cpp\
        mainwindow.cpp \
    logindialog.cpp \
    registerdialog.cpp \
    memberclient.cpp \
    credentials.cpp \
    socket.cpp \
    exception.cpp \
    command.cpp \
    commandreader.cpp \
    commandsender.cpp \
    chatbox.cpp \
    cmdthreadsender.cpp \
    cmdthreadreceiver.cpp \
    dialogsmanager.cpp \
    bubbletextbrowser.cpp \
    customsearchlistwidgetitem.cpp \
    clientsqlitecontroller.cpp \
    sssqlitecontroller.cpp \
    customfriendlistwidgetitem.cpp \
    membersmanager.cpp \
    archive.cpp

HEADERS  += mainwindow.h \
    logindialog.h \
    registerdialog.h \
    memberclient.h \
    credentials.h \
    socket.h \
    exception.h \
    commandenum.h \
    command.h \
    commandreader.h \
    commandsender.h \
    statics.h \
    chatbox.h \
    cmdthreadsender.h \
    cmdthreadreceiver.h \
    dialogsmanager.h \
    bubbletextbrowser.h \
    customsearchlistwidgetitem.h \
    clientsqlitecontroller.h \
    sssqlitecontroller.h \
    customfriendlistwidgetitem.h \
    membersmanager.h \
    archive.h

FORMS    += mainwindow.ui \
    logindialog.ui \
    registerdialog.ui \
    chatbox.ui \
    archive.ui

RESOURCES += \
    res.qrc
