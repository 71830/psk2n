#include "command.h"
#include <exception.h>
#include <statics.h>
/*
Command::Command(const char * message, CommandType commandType, const char * from, const char * to)
{
    this->message = message;
    this->type = commandType;
    this->from = from;
    this->to = to;
}
*/
Command::Command(const std::string message, CommandType commandType, const std::string from, std::string to)
{

    char * msg = new char[message.length() + 1];
    msg[message.length()] = '\0';
    strcpy(msg, message.c_str());

    this->message = msg;

    this->type = commandType;

    char * fr = new char[UserNameBufSize + 1];
    fr[UserNameBufSize] = '\0';
    strcpy(fr, from.c_str());

    this->from = fr;

    char * t = new char[UserNameBufSize + 1];
    t[UserNameBufSize] = '\0';
    strcpy(t, to.c_str());

    this->to = t;

}

bool Command::validate(){
    return !(this->getType() == -1 || this->getMessage() == nullptr || this->getFrom() == nullptr || this->getTo() == nullptr);
}


CommandType Command::getType(){
    return this->type;
}

const char * Command::getMessage(){
    return this->message;
}

const char * Command::getFrom(){
    return this->from;
}

const char * Command::getTo(){
    return this->to;
}


Command::~Command(){
    if(from != NULL){
        delete from;
    }
    if(to != NULL){
        delete to;
    }
    if(message != NULL){
        delete message;
    }
}
