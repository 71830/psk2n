#ifndef SSSQLITECONTROLLER_H
#define SSSQLITECONTROLLER_H
#include <sqlite3.h>

#include <iostream>
#include <string>
#include <vector>

class SSSqliteController
{
public:
    SSSqliteController(std::string & name, std::string & createQuery);
    virtual ~SSSqliteController();

    sqlite3 *getDB();//



private:
    sqlite3 *db;
    bool databaseExists(const std::string& name);//
};

#endif // SSSQLITECONTROLLER_H
