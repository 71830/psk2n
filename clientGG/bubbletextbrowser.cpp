#include "bubbletextbrowser.h"
#include <QAbstractTextDocumentLayout>
#include <QtGui>
BubbleTextBrowser:: BubbleTextBrowser(QColor color):
    QTextBrowser(0)
{
    this->setStyleSheet("QTextBrowser { border-radius: 5px; background-color: " + color.name() + ";}");

    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);


    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    connect(this->document()->documentLayout(), &QAbstractTextDocumentLayout::documentSizeChanged, this, &BubbleTextBrowser::OnDocumentSizeChanged);
}

BubbleTextBrowser::~BubbleTextBrowser(){

}

void BubbleTextBrowser::OnDocumentSizeChanged(const QSizeF &newSize){
    qDebug() << newSize.width();
    this->setMinimumHeight(newSize.height());
    this->setMaximumHeight(newSize.height());
    this->setMinimumWidth(this->document()->idealWidth());
    this->setMaximumWidth(this->document()->idealWidth());
}
