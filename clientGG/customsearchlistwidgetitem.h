#ifndef CUSTOMSEARCHLISTWIDGETITEM_H
#define CUSTOMSEARCHLISTWIDGETITEM_H

#include <QListWidgetItem>
#include <QLabel>
#include <QToolButton>
#include <QListWidget>

class CustomSearchListWidgetItem : public QListWidgetItem
{

public:
    explicit CustomSearchListWidgetItem(QString labelText, QListWidget *parent);
    ~CustomSearchListWidgetItem(){
        //delete label;
        //delete button;
        //delete widget;
    }

public:
    QToolButton * getButton(){return this->button;}

private:
    QLabel * label;
    QToolButton * button;
    QWidget * widget;


};

#endif // CUSTOMSEARCHLISTWIDGETITEM_H
