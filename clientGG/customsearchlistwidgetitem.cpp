#include "customsearchlistwidgetitem.h"
#include <QHBoxLayout>

CustomSearchListWidgetItem::CustomSearchListWidgetItem(QString labelText, QListWidget *parent) :
    QListWidgetItem(parent)
{
    QHBoxLayout * lay = new QHBoxLayout(this->listWidget());

    this->label = new QLabel(labelText);
    lay->addWidget(this->label, 0, Qt::AlignLeft);

    this->button = new QToolButton();
    QIcon icon(":/images/plus.png");

    button->setIconSize(QSize(25,25));
    button->setIcon(icon);
    button->setToolButtonStyle(Qt::ToolButtonIconOnly);
    button->setText(labelText);
    lay->addWidget(button, 0, Qt::AlignRight);

    this->widget = new QWidget();
    this->widget->setStyleSheet("QWidget { border-bottom: 1px solid #DDDDDD; } ");
    this->widget->setLayout(lay);

    this->setSizeHint(this->widget->sizeHint());

    parent->setItemWidget(this, this->widget);
}
