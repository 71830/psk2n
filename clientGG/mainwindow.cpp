#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_mainwindow.h"
#include "logindialog.h"
#include "registerdialog.h"
#include <stdio.h>
#include <QMessageBox>
#include <commandsender.h>
#include <cmdthreadsender.h>
#include <QToolButton>

#include <QtGui>
#include <customsearchlistwidgetitem.h>

#include <clientsqlitecontroller.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QRegExp re("[0-9a-zA-Z]+");
    QRegExpValidator * validator = new QRegExpValidator(re, this);
    this->ui->searchFriendLineEdit->setValidator(validator);
    //this->ui->ownerNameAction->setCheckable(false);
    this->ui->ownerNameAction->setEnabled(false);
    connect(this->ui->friendsListWidget, &QListWidget::itemDoubleClicked, this, &MainWindow::OnFriendListItemDoubleClicked);


    try{
        socket = Socket("127.0.0.1", 8008);
    } catch (Exception & ex){
        QMessageBox::warning(this, "Creating socket error", QString::fromStdString(ex.getErrorMessage()), QMessageBox::Close);
        this->close();
    }

    this->setSearchGuiVisibleAndEnabled(false);
    this->setFriendsGuiVisibleAndEnabled(true);

    this->login();


}
MainWindow::~MainWindow()
{
    delete ui;
    if(receiver != NULL){
        receiver->stop();
        receiver->wait();
        delete receiver;
    }
    if(membersManager != NULL){
        delete membersManager;
    }
    if(archive != NULL){
        delete archive;
    }

}

Socket MainWindow::socket;

Socket & MainWindow::GetSocket(){
    return socket;
}

void MainWindow::login(){

    LoginDialog loginForm;
    bool looper = true;

    while(looper){
        if(loginForm.exec() == QDialog::Accepted){
            if(loginForm.getDialogResult() == LoginDialog::LOGGING_SUCCESS){

                this->username = loginForm.getUsername();
                this->ui->ownerNameAction->setText(QString(this->username.c_str()));
                this->connectAndStartReceiver();

                this->connectToDataBase();

                this->initiateMembersManagerAndFriendsWidget(loginForm.getLoggedMembersResponse());

                QMessageBox::information(this, "Logging successful", "You have been logged in.", QMessageBox::Ok, 0);
                looper = false;

            } else if(loginForm.getDialogResult() == LoginDialog::REGISTER) {
                RegisterDialog registerForm;

                if(registerForm.exec() == QDialog::Accepted){
                    QMessageBox::information(this, "Register successful", "You have been registered. Now you can sign in.", QMessageBox::Ok, 0);
                }

            }
        }
        else {
            looper = false;
            this->close();
        }
    }
}

void MainWindow::initiateMembersManagerAndFriendsWidget(QString membersListFromLogging){
    this->membersManager = new MembersManager(membersListFromLogging, this);
    connect(this->membersManager, &MembersManager::userLoggedChanged, this, &MainWindow::OnMemberSetLogged);
    connect(this->membersManager, &MembersManager::userAdded, this, &MainWindow::OnMemberAddedToMembersManager);
    connect(this->membersManager, &MembersManager::userRemoved, this, &MainWindow::OnMemberRemovedFromMembersManager);

    this->addMembersToFriendsListWidget();
}

void MainWindow::addMembersToFriendsListWidget(){

    for(std::set<std::string>::iterator it = this->membersManager->getLoggedMembers().begin(); it != this->membersManager->getLoggedMembers().end(); ++it){
        CustomFriendListWidgetItem * item = new CustomFriendListWidgetItem(QString((*it).c_str()), true, this->ui->friendsListWidget);
        connect(item->getRemoveButton(), &QToolButton::clicked, this, &MainWindow::OnRemoveFriendToolButtonClicked);
    }
    for(std::set<std::string>::iterator it = this->membersManager->getNotLoggedMembers().begin(); it != this->membersManager->getNotLoggedMembers().end(); ++it){

        CustomFriendListWidgetItem * item = new CustomFriendListWidgetItem(QString((*it).c_str()), false, this->ui->friendsListWidget);
        connect(item->getRemoveButton(), &QToolButton::clicked, this, &MainWindow::OnRemoveFriendToolButtonClicked);
    }
}

void MainWindow::connectAndStartReceiver(){
    this->receiver = new CmdThreadReceiver(GetSocket().getDescriptor());
    connect(this->receiver, &CmdThreadReceiver::resultReady, this, &MainWindow::OnCommandReceive);
    this->receiver->start();
}

void MainWindow::OnCommandReceive(int error, char * errmsg, Command * cmd){
    qDebug() << "Receiver thread id " << QThread::currentThreadId();
    if(error < 0)
        QMessageBox::information(this, "Message received error", QString(errmsg), QMessageBox::StandardButton::Ok, 0, 0);
    else{
        switch(cmd->getType()){
            case SEND_MESSAGE:{
                std::string from(cmd->getFrom());
                std::string message(cmd->getMessage());
                ClientSqliteController::getInstance(this->getOwnerName()).addMessage(from, this->getOwnerName(), message);
                this->getDialogsManager().checkIfDialogExistsAndAddMessageFromSender(cmd->getMessage(), cmd->getFrom(), this->getOwnerName());
                break;
        } case USERS_SEARCH:{
                this->addSearchedUsers(cmd->getMessage());
                break;
        } case USERS_SEARCH_NOT_FOUND:{
                QMessageBox::information(this, "Search friends", "No users found", QMessageBox::StandardButton::Ok, 0,0);
            break;
        } case FRIEND_ADDED:{
            this->addFriend(cmd);
            break;
        } case FRIEND_NOT_ADDED:{
            QMessageBox::information(this, "Member adding error", QString(cmd->getMessage()), QMessageBox::StandardButton::Ok, 0,0);
            break;
        } case FRIEND_REMOVED:{
            this->removeFriend(cmd);
            break;
        } case FRIEND_NOT_REMOVED:{
            QMessageBox::information(this, "Member removing error", QString(cmd->getMessage()), QMessageBox::StandardButton::Ok, 0,0);
            break;
        } case USER_LOGGED:{
            this->membersManager->setLogged(cmd->getMessage(), true);
            break;
        } case USER_LOGGED_OUT:{
            this->membersManager->setLogged(cmd->getMessage(), false);
            break;
        }
        default:{
            break;
            }
        }
    }
    delete cmd;
}

void MainWindow::addFriend(Command * cmd){
    // add to list logic logged from message
    QString str(cmd->getMessage());
    QStringList l = str.split("||", QString::SplitBehavior::SkipEmptyParts);
    if(l.size() == 2){
        std::string member = l[0].toStdString();
        int logged = l[1].toInt();


        if(!ClientSqliteController::getInstance(this->getOwnerName()).memberExists(member)){
            if(!ClientSqliteController::getInstance(this->getOwnerName()).addMember(member)){
                QMessageBox::information(this, "Add member error", "Member added on server, but error occurred on client. Log out and then log in to refresh data.", QMessageBox::Ok, 0, 0);
            } else {
                this->membersManager->addMember(member, logged);
            }
        }
    }
}

void MainWindow::removeFriend(Command* cmd){
    std::string member = cmd->getMessage();
    ClientSqliteController::getInstance(this->getOwnerName()).removeMember(member);
    this->membersManager->removeMember(member);


}

void MainWindow::addSearchedUsers(const char * message){
    QString str(message);
    if(str.isNull() || str.isEmpty()){
        QMessageBox::information(this, "Search friends", "Users list not found in response.", QMessageBox::StandardButton::Ok, 0, 0);
    } else {


        QStringList list = str.split("||", QString::SplitBehavior::SkipEmptyParts);
        list.removeOne(this->getOwnerName().c_str());

        if(list.size() == 0){
            QMessageBox::information(this, "Search friends", "Users list not found in response.", QMessageBox::StandardButton::Ok, 0, 0);
        }else {
            this->ui->searchFriendListWidget->clear();

            for(int i = 0; i < list.size(); i++){
                std::string user = list[i].toStdString();
                if(!ClientSqliteController::getInstance(this->getOwnerName()).memberExists(user)){
                   CustomSearchListWidgetItem * item = new CustomSearchListWidgetItem(list[i], this->ui->searchFriendListWidget);
                   connect(item->getButton(), &QToolButton::clicked, this, &MainWindow::OnAddFriendToolButtonClicked);
                }
            }
        }

    }

}

void MainWindow::OnAddFriendToolButtonClicked(){
   QToolButton * addFriendButton = (QToolButton *)sender();

   Command * addFriendCommand = new Command(addFriendButton->text().toStdString(), CommandType::ADD_FRIEND, this->getOwnerName(), "Server");
   this->createSendingThreadAndSendCommand(addFriendCommand);
}

void MainWindow::OnRemoveFriendToolButtonClicked(){
   QToolButton * removeFriendButton = (QToolButton *)sender();

   Command * removeFriend = new Command(removeFriendButton->text().toStdString(), CommandType::REMOVE_FRIEND, this->getOwnerName(), "Server");
   this->createSendingThreadAndSendCommand(removeFriend);
}

std::string &MainWindow::getOwnerName(){
    return this->username;
}

DialogsManager & MainWindow::getDialogsManager(){
    return this->dialogsManager;
}

void MainWindow::connectToDataBase(){
    try{
        ClientSqliteController::getInstance(this->getOwnerName());
    } catch (Exception & ex){
        qDebug() << "Data base error \n" << ex.getErrorMessage().c_str() << " " <<ex.getSystemErrorMessage().c_str();
        int result = QMessageBox::warning(this, "Data base error", "Data base error occured", QMessageBox::StandardButton::Ok, 0,0);
        if(result == QMessageBox::Ok){
            this->close();
        }
    }
}

void MainWindow::on_addFriendAction_triggered()
{
    this->setSearchGuiVisibleAndEnabled(true);
    this->setFriendsGuiVisibleAndEnabled(false);
}


void MainWindow::setSearchGuiVisibleAndEnabled(bool value){
    setWidgetVisibleAndEnabled(this->ui->searchWidget, value);
}

void MainWindow::setFriendsGuiVisibleAndEnabled(bool value){
    setWidgetVisibleAndEnabled(this->ui->friendsWidget, value);
}



void MainWindow::setWidgetVisibleAndEnabled(QWidget * widget, bool value){
    widget->setVisible(value);
    widget->setEnabled(value);
}


void MainWindow::setWidgetVisibleAndEnabled(QWidget * widget, bool visible, bool enabled){
    widget->setVisible(visible);
    widget->setEnabled(enabled);
}

void MainWindow::on_friendsAction_triggered()
{
    this->setFriendsGuiVisibleAndEnabled(true);
    this->setSearchGuiVisibleAndEnabled(false);
}

void MainWindow::on_searchFriendPushButton_clicked()
{
    if(!this->ui->searchFriendLineEdit->text().isEmpty()){

      Command * cmd = new Command(this->ui->searchFriendLineEdit->text().toStdString(), CommandType::USERS_SEARCH, this->getOwnerName(), "Server");
      this->createSendingThreadAndSendCommand(cmd);

    } else {
        QMessageBox::information(this, "Searching error", "Type name of user or part of it.", QMessageBox::StandardButton::Ok, 0);
    }
}

void MainWindow::OnCommandSent(int error, const char * errmsg, Command * cmd){
    if(error < 0){
        QMessageBox::information(this, "Sending command error", "Error encountered when sending command: " + QString::number(cmd->getType()) + ". " + QString(errmsg), QMessageBox::StandardButton::Ok, 0);
    }

    QObject * s = sender();
    delete s;
}

void MainWindow::OnMemberSetLogged(std::string /*username*/, bool /*logged*/){
    this->ui->friendsListWidget->clear();
    this->addMembersToFriendsListWidget();
}

void MainWindow::OnMemberAddedToMembersManager(std::string /*username*/, bool /*logged*/){
    this->ui->friendsListWidget->clear();
    this->addMembersToFriendsListWidget();
}

void MainWindow::OnMemberRemovedFromMembersManager(std::string /*username*/){
    this->ui->friendsListWidget->clear();
    this->addMembersToFriendsListWidget();
}

void MainWindow::OnFriendListItemDoubleClicked(QListWidgetItem * item){
    CustomFriendListWidgetItem * cflwi = (CustomFriendListWidgetItem*)item;
    std::string username = cflwi->getRemoveButton()->text().toStdString();
    this->dialogsManager.createAndAddNewChatBox(username, this->getOwnerName());
}



void MainWindow::createSendingThreadAndSendCommand(Command * cmd){
    CmdThreadSender *thread = new CmdThreadSender(cmd,  MainWindow::GetSocket().getDescriptor());
    connect(thread, &CmdThreadSender::resultReady, this, &MainWindow::OnCommandSent);
    thread->start();
}



void MainWindow::on_archiveIcon_triggered()
{
    if(archive == NULL){
        archive = new Archive(this->getOwnerName());
    }
    archive->show();
}
