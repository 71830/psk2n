#include "customfriendlistwidgetitem.h"
#include <QHBoxLayout>
CustomFriendListWidgetItem::CustomFriendListWidgetItem(QString labelText, bool logged, QListWidget *parent)
    : QListWidgetItem(parent)
{
    QHBoxLayout * lay = new QHBoxLayout(this->listWidget());
    lay->setSpacing(0);
    lay->setMargin(0);
    lay->setContentsMargins(0,0,0,0);

    this->label = new QLabel(labelText);
    lay->addWidget(this->label, 0, Qt::AlignLeft);


    QIcon rIcon(":/images/removeFriend.png");


    this->removeButton = new QToolButton();
    this->removeButton->setIconSize(QSize(25,25));
    this->removeButton->setIcon(rIcon);
    this->removeButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
    this->removeButton->setText(labelText);
    this->removeButton->setStyleSheet("QToolButton { background: rgba(0,0,0,0);}");


    this->icon = new QIcon(":/images/logged.png");


    this->iconLabel = new QLabel();

    this->iconLabel->setMinimumSize(25, 25);
    this->iconLabel->setMaximumSize(25, 25);

    this->setLogged(logged);

    lay->addStretch();

    lay->addWidget(this->iconLabel, 0, Qt::AlignRight);
    lay->addWidget(this->removeButton, 0, Qt::AlignRight);

    this->widget = new QWidget();
    this->widget->setStyleSheet("QWidget { border-bottom: 1px solid #DDDDDD; } ");
    this->widget->setLayout(lay);

    this->setSizeHint(this->widget->sizeHint());

    parent->setItemWidget(this, this->widget);
}

void CustomFriendListWidgetItem::setLogged(bool logged){
    QIcon::Mode mode = logged ? QIcon::Mode::Active : QIcon::Mode::Disabled;
    this->iconLabel->setPixmap(this->icon->pixmap(24,24, mode, QIcon::State::On));
}
