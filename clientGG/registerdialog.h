#ifndef REGISTERDIALOG_H
#define REGISTERDIALOG_H

#include <QDialog>
#include <credentials.h>

namespace Ui {
class RegisterDialog;
}

class RegisterDialog : public QDialog
{
    Q_OBJECT

public:
    enum DialogResult { REGISTER_SUCCESS, REGISTER_FAILED };
    explicit RegisterDialog(QWidget *parent = 0);
    ~RegisterDialog();
    Credentials getCredetials();

private slots:
    void on_registerButton_clicked();

private:
    Ui::RegisterDialog *ui;
    void registerUser();
    void registerCommand(int fd);
    bool validate();
    DialogResult dialogResultValue;
    Credentials credentials;
};

#endif // REGISTERDIALOG_H
