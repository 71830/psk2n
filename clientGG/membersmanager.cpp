#include "membersmanager.h"
#include <QStringList>

MembersManager::MembersManager(QString membersAndLogged, QObject * parent) : QObject(parent)
{
    QStringList list = membersAndLogged.split("||", QString::SplitBehavior::SkipEmptyParts);
    for(int i = 0; i < list.size(); i++){
        QStringList user = list[i].split("&", QString::SplitBehavior::KeepEmptyParts);
        this->addMember(user[0].toStdString(), user[1] == "1" ? true : false);
    }
}



void MembersManager::addMember(std::string username, bool logged){
    if(this->loggedMembers.find(username) != this->loggedMembers.end()){
        if(!logged){
            this->loggedMembers.erase(username);
            this->notLoggedMembers.insert(username);
        }
    }
    else if(this->notLoggedMembers.find(username) != this->notLoggedMembers.end()){
        if(logged){
            this->notLoggedMembers.erase(username);
            this->loggedMembers.insert(username);
        }
    } else {
        if(logged){
            this->loggedMembers.insert(username);
        } else {
            this->notLoggedMembers.insert(username);
        }
    }
    emit this->userAdded(username, logged);
}

void MembersManager::removeMember(std::string username){
    this->loggedMembers.erase(username);
    this->notLoggedMembers.erase(username);
    emit this->userRemoved(username);
}

void MembersManager::setLogged(std::string username, bool logged){
    if(logged){
        this->loggedMembers.insert(username);
        this->notLoggedMembers.erase(username);
    } else {
        this->notLoggedMembers.insert(username);
        this->loggedMembers.erase(username);
    }
    emit this->userLoggedChanged(username, logged);
}

