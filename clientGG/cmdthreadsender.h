#ifndef CMDTHREADSENDER_H
#define CMDTHREADSENDER_H
#include <QThread>
#include <command.h>
#include <commandsender.h>
#include <mutex>
#include <string.h>
#include <QDebug>
//check if delete remove cmd
class CmdThreadSender : public QThread
{

    Q_OBJECT
    void run() Q_DECL_OVERRIDE {
        static std::mutex locker;
        locker.lock();
        qDebug() << "Sender thread id " << QThread::currentThreadId();
        int k = CommandSender(fd).sendCommand(cmd);

        emit resultReady(k, strerror(errno), cmd);
        locker.unlock();
    }

signals:
    void resultReady(int error,const char * errmsg, Command * cmd);


public:
    CmdThreadSender(Command * cmd, int fd);
    ~CmdThreadSender(){
        delete cmd;
    }

private:
    Command * cmd;
    int fd;
};

#endif // CMDTHREADSENDER_H
