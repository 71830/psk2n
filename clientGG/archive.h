#ifndef ARCHIVE_H
#define ARCHIVE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QTreeWidgetItem>

namespace Ui {
class Archive;
}

class Archive : public QWidget
{
    Q_OBJECT

public:

    explicit Archive(std::string name, QWidget *parent = 0);
    ~Archive();
    void show();

public slots:
    void OnMemberWidgetItemClicked(QTreeWidgetItem * item, int column);

private:
    Ui::Archive *ui;
    std::string ownername;

    QVBoxLayout * scrollLayout = NULL;

    void clearChildOfScrollLayout();
    void addMemberListToTreeWidget(std::vector<std::string> l, bool known);
    void addFromOwner(QString formatted);
    void addFromSender(QString message, std::string sender);
    void addDateLabel(QString str);

};

#endif // ARCHIVE_H
