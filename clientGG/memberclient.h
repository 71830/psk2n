#ifndef MEMBERCLIENT_H
#define MEMBERCLIENT_H
#include <string>
class MemberClient
{
public:
    MemberClient(std::string name, bool logged);
    std::string getUsername();
    void setUsername(std::string name);
    bool isLogged();
    void setIsLogged(bool logged);
private:
    bool logged;
    std::string username;

};

#endif // MEMBERCLIENT_H
