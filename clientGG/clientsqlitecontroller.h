#ifndef CLIENTSQLITECONTROLLER_H
#define CLIENTSQLITECONTROLLER_H
#include <sssqlitecontroller.h>
#include <QString>
#include <QStringList>
class ClientSqliteController : public SSSqliteController
{
public:
   static ClientSqliteController & getInstance(std::string name);

    bool addMessage(std::string & from, std::string & to, std::string & message);
    bool addMember(std::string & name);
    bool removeMember(std::string & name);
    bool memberExists(std::string & name);
    void checkServerMembersSynchronized(QString members);
    std::vector<std::string> getMembers();
    std::vector<std::string> getUnknownMembers(std::string ownername);

    std::vector<std::vector<std::string>> getMessages(std::string &toname);


private:
    ClientSqliteController(std::string & name, std::string & createQuery);



};

#endif // CLIENTSQLITECONTROLLER_H
