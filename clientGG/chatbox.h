#ifndef CHATBOX_H
#define CHATBOX_H

#include <QWidget>
#include <isender.h>
#include <QVBoxLayout>
#include <QTextBrowser>
namespace Ui {
class ChatBox;
}

class ChatBox : public QWidget
{
    Q_OBJECT

public:
    explicit ChatBox(std::string username, std::string owner, QWidget *parent = 0);
    ~ChatBox();

    void addNewMessageFromYouToScrollArea(QString formatted);
    void addMessageFromSenderToScrollArea(const char * message);

private slots:
    void on_sendMessageButton_clicked();
    void OnMessageSent(int error, const char * errmsg, Command * cmd);

    void on_writeMessageAreaTextEdit_textChanged();

    void moveScrollBarToBottom(int min, int max);

    void on_boldButton_clicked();

    void on_italicButton_clicked();

    void on_underLineButton_clicked();

private:
    void addDateLabel();
    QString formatTextToSend();

    Ui::ChatBox *ui;
    std::string username;
    std::string owner;

    bool bold = false, italic = false, underline = false;

    QVBoxLayout * scrollLayout;
};

#endif // CHATBOX_H
