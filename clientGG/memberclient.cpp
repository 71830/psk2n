#include "memberclient.h"

MemberClient::MemberClient(std::string name, bool logged)
{
    this->setUsername(name);
    this->setIsLogged(logged);
}

std::string MemberClient::getUsername(){
    return this->username;
}

void MemberClient::setUsername(std::string name){
    this->username = name;
}

bool MemberClient::isLogged(){
    return this->logged;
}

void MemberClient::setIsLogged(bool logged){
    this->logged = logged;
}
