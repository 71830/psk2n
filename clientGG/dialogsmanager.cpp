#include "dialogsmanager.h"


DialogsManager::DialogsManager()
{

}


bool DialogsManager::chatBoxExists(std::string username){
    DialogsManager::DialogMap::const_iterator searched = this->getDialogs().find(username);
    return (searched != this->getDialogs().end());
}

ChatBox * DialogsManager::getChatBox(std::string username){
    DialogsManager::DialogMap::const_iterator searched = this->getDialogs().find(username);
    if(searched != this->getDialogs().end()){
        return searched->second;
    }
    return nullptr;
}


void DialogsManager::addChatBox(std::string username, ChatBox * chatbox){
    this->getDialogs().emplace(username, chatbox);
    chatbox->show();
}

int DialogsManager::removeChatBox(std::string username){
   return this->getDialogs().erase(username);
}

int DialogsManager::getCount(){
    return this->getDialogs().size();
}


DialogsManager::DialogMap & DialogsManager::getDialogs(){
    return this->dialogs;
}
void DialogsManager::createDialogAndAddMessageFromSender(const char * message, std::string username, std::string owner){
    ChatBox * box = new ChatBox(username, owner);
    this->addChatBox(username, box);
    box->addMessageFromSenderToScrollArea(message);
}

void DialogsManager::checkIfDialogExistsAndAddMessageFromSender(const char * message, std::string username, std::string owner){
    ChatBox * chatBox = this->getChatBox(username);
    if(chatBox != nullptr){
        chatBox->addMessageFromSenderToScrollArea(message);
        chatBox->show();
    } else {
        this->createDialogAndAddMessageFromSender(message, username, owner);
    }
}

void DialogsManager::createAndAddNewChatBox(std::string username, std::string owner){
    if(!chatBoxExists(username)){
        ChatBox * chatbox = new ChatBox(username, owner);
        this->addChatBox(username, chatbox);
    } else {
        getChatBox(username)->show();
    }
}
