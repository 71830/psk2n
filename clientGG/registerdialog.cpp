#include "registerdialog.h"
#include "ui_registerdialog.h"
#include <QMessageBox>
#include <mainwindow.h>
#include <commandenum.h>
#include <command.h>
#include <commandreader.h>
#include <commandsender.h>

RegisterDialog::RegisterDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegisterDialog)
{
    ui->setupUi(this);

    QRegExp re("[0-9a-zA-Z]+");
    QRegExpValidator * validator = new QRegExpValidator(re, this);
    this->ui->passwordLineEdit->setValidator(validator);
    this->ui->repeatPasswordLineEdit->setValidator(validator);
    this->ui->usernameLineEdit->setValidator(validator);

}

RegisterDialog::~RegisterDialog()
{
    delete ui;
}

void RegisterDialog::on_registerButton_clicked()
{
    this->registerUser();
}

void RegisterDialog::registerUser(){
    if(validate()){
        Socket socket = MainWindow::GetSocket();

        if(socket.isConnected()){
            this->registerCommand(socket.getDescriptor());
        } else {
            try{
                socket.connectSocket();
                this->registerCommand(socket.getDescriptor());
            } catch (Exception & ex){
               int result = QMessageBox::warning(this, "Connecting error", QString::fromStdString(ex.getErrorMessage()), QMessageBox::Retry, QMessageBox::Cancel);
               if(result == QMessageBox::Retry){
                   this->registerUser();
               }
            }
        }
    }
}

void RegisterDialog::registerCommand(int fd){

    Command * cmd = new Command(this->credentials.getPassword(),CommandType::REGISTER, this->credentials.getUsername(), "Server");
    CommandSender(fd).sendCommand(cmd);
    Command * commandRegisterResult = CommandReader(fd).readCommand();

    if(commandRegisterResult->getType() == CommandType::REGISTER_SUCCESS){
        this->dialogResultValue = RegisterDialog::DialogResult::REGISTER_SUCCESS;
        this->accept();
    } else if(commandRegisterResult->getType() == CommandType::REGISTER_FAILED) {
        int result = QMessageBox::warning(this, "Register error", QString::fromStdString(commandRegisterResult->getMessage()), QMessageBox::Retry, QMessageBox::Cancel);
        if(result == QMessageBox::Retry){
            this->registerUser();
        }
    }

    delete cmd;
    delete commandRegisterResult;
}

bool RegisterDialog::validate(){
    QMessageBox message;
    message.setWindowTitle("Register error");
    if(ui->usernameLineEdit->text().isEmpty()){
        message.setText("Username cannot be empty!");
        message.exec();
    } else if (ui->passwordLineEdit->text().isEmpty()){
        message.setText("Password cannot be empty!");
        message.exec();
    } else if(ui->passwordLineEdit->text().compare(ui->repeatPasswordLineEdit->text(),Qt::CaseSensitive) != 0){
        message.setText("Given passwords do not match!");
        message.exec();

    } else{
        this->credentials = Credentials(this->ui->usernameLineEdit->text().toStdString(), this->ui->passwordLineEdit->text().toStdString());
        return true;
    }
    return false;
}


Credentials RegisterDialog::getCredetials(){
    return this->credentials;
}
