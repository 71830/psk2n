#ifndef SOCKET_H
#define SOCKET_H

#include <sys/socket.h>
#include <sys/unistd.h>
#include <netinet/in.h>
#include <exception.h>
#include <errno.h>
#include <arpa/inet.h>

class Socket
{
private:
    int socketDesc;
    sockaddr_in socketAddress;
public:
    Socket(int socketDescriptor, sockaddr_in socketAddress);

    Socket(const char * ip, int port);

    Socket(){}



    int getDescriptor();
    sockaddr_in * getSocketAddress();
    char * getIp();
    int getPort();
    int connectSocket();
    void closeSocket();
    bool isConnected();
};

#endif // SOCKET_H
