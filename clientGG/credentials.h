#ifndef CREDENTIALS_H
#define CREDENTIALS_H
#include <string>
class Credentials
{
public:
    Credentials(std::string username, std::string password);
    std::string getUsername();
    std::string getPassword();
    Credentials(){;}
private:
    std::string username;
    std::string password;


};

#endif // CREDENTIALS_H
